/*
 * gdeckstyledefault.c
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gdeckstyledefault.h"
#include "gdeckstyledefaultpaths.h"
#include "libart_lgpl/art_vpath.h"
#include "libart_lgpl/art_bpath.h"
#include "libart_lgpl/art_vpath_bpath.h"
#include "libart_lgpl/art_svp.h"
#include "libart_lgpl/art_svp_vpath.h"
#include "libart_lgpl/art_rgb_svp.h"
#include "art_render.h"
#include "art_render_svp.h"
#include <string.h>

static void       gdeck_style_default_init             (GDeckStyleDefault      *style);
static void       gdeck_style_default_class_init       (GDeckStyleDefaultClass *class);

static GdkPixbuf *gdeck_real_style_default_get_mask    (GDeckStyle             *style);
static GdkPixbuf *gdeck_real_style_default_get_blank   (GDeckStyle             *style);
static void       gdeck_real_style_default_render_slot (GDeckStyle             *style,
							GdkPixbuf              *pixbuf,
							GDeckSlotType           type,
							GDeckSlotLayout         layout,
							GDeckState              state);
static void       gdeck_real_style_default_render_card (GDeckStyle             *style,
							GdkPixbuf              *pixbuf,
							GDeckCard              *card,
							gint                    x,
							gint                    y,
							GDeckSlotLayout         type,
							GDeckState              state,
							gboolean                next_card);



static GObjectClass *parent_class = NULL;


GType
gdeck_style_default_get_type (void)
{
     static GType style_type = 0;

     if (! style_type)
     {
	  static const GTypeInfo style_info =
	  {
	       sizeof (GDeckStyleDefaultClass),
	       NULL,           /* base_init */
	       NULL,           /* base_finalize */
	       (GClassInitFunc) gdeck_style_default_class_init,
	       NULL,           /* class_finalize */
	       NULL,           /* class_data */
	       sizeof (GDeckStyleDefault),
	       0,             /* n_preallocs */
	       (GInstanceInitFunc) gdeck_style_default_init,
	  };

	  style_type = g_type_register_static (GDECK_TYPE_STYLE, "GDeckStyleDefault", &style_info, 0);
     }

     return style_type;
}

/* object members */
static void
gdeck_style_default_init (GDeckStyleDefault *style)
{
  style->radius = 5;
  style->outline_color = 0x000000FF;
  style->slot_highlight_color = 0x00000080;
  style->slot_fill_color = 0xFFFFFF80;
  style->slot_selected_color = 0x00000080;
  style->slot_stock_used_color = 0x0000030;
  style->card_fill_color = 0xFFFFFFFF;
}

static void
gdeck_style_default_class_init (GDeckStyleDefaultClass *class)
{
     GDeckStyleClass *style_class = (GDeckStyleClass *) class;

     parent_class = g_type_class_peek_parent (class);

     style_class->get_mask = gdeck_real_style_default_get_mask;
     style_class->get_blank = gdeck_real_style_default_get_blank;
     style_class->render_slot = gdeck_real_style_default_render_slot;
     style_class->render_card = gdeck_real_style_default_render_card;
}

GDeckStyle *
gdeck_style_default_new (void)
{
  return GDECK_STYLE (g_object_new (GDECK_TYPE_STYLE_DEFAULT, NULL));
}

static GdkPixbuf *
gdeck_real_style_default_get_mask (GDeckStyle *style)
{
  GDeckStyleDefault *default_style = GDECK_STYLE_DEFAULT (style);

  if (default_style->mask == NULL)
    {
      ArtRender *render;
      guint rgb;
      ArtPixMaxDepth color[4];
      GdkPixbuf *pixbuf;
      pixbuf = default_style->mask = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
						     TRUE, 8,
						     style->width, style->height);

      memset (gdk_pixbuf_get_pixels (default_style->mask),
	      0x00000000,
	      gdk_pixbuf_get_rowstride (default_style->mask) *
	      gdk_pixbuf_get_height (default_style->mask));

      rgb = 0xffffff;
      color[0] = ART_PIX_MAX_FROM_8 (rgb >> 16);
      color[1] = ART_PIX_MAX_FROM_8 ((rgb >> 8) & 0xff);
      color[2] = ART_PIX_MAX_FROM_8 (rgb  & 0xff);

      render = art_render_new (0, 0, style->width, style->height,
			       gdk_pixbuf_get_pixels (pixbuf),
			       gdk_pixbuf_get_rowstride (pixbuf),
			       gdk_pixbuf_get_n_channels (pixbuf) - 1,
			       gdk_pixbuf_get_bits_per_sample (pixbuf),
			       ART_ALPHA_PREMUL,
			       NULL);
      art_render_image_solid (render, color);
      art_render_svp (render, default_style->fill_svp);
      art_render_invoke (render);

      rgb = 0x000000;
      color[0] = ART_PIX_MAX_FROM_8 (rgb >> 16);
      color[1] = ART_PIX_MAX_FROM_8 ((rgb >> 8) & 0xff);
      color[2] = ART_PIX_MAX_FROM_8 (rgb  & 0xff);
      
      render = art_render_new (0, 0, style->width, style->height,
			       gdk_pixbuf_get_pixels (pixbuf),
			       gdk_pixbuf_get_rowstride (pixbuf),
			       gdk_pixbuf_get_n_channels (pixbuf) - 1,
			       gdk_pixbuf_get_bits_per_sample (pixbuf),
			       ART_ALPHA_PREMUL,
			       NULL);
      art_render_image_solid (render, color);
      art_render_svp (render, default_style->outline_svp);
      art_render_invoke (render);
      gdk_pixbuf_save (default_style->mask, "mask.png", "png", NULL, NULL, NULL);
    }

  return default_style->mask;
}

static GdkPixbuf *
gdeck_real_style_default_get_blank (GDeckStyle *style)
{
  GDeckStyleDefault *default_style = GDECK_STYLE_DEFAULT (style);

  if (default_style->blank == NULL)
    {
      ArtRender *render;
      guint rgb;
      ArtPixMaxDepth color[4];

      default_style->blank = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
					     TRUE, 8,
					     style->width, style->height);

      memset (gdk_pixbuf_get_pixels (default_style->blank),
	      0x00000000,
	      gdk_pixbuf_get_rowstride (default_style->blank) *
	      gdk_pixbuf_get_height (default_style->blank));

      rgb = 0xffffff;
      color[0] = ART_PIX_MAX_FROM_8 (rgb >> 16);
      color[1] = ART_PIX_MAX_FROM_8 ((rgb >> 8) & 0xff);
      color[2] = ART_PIX_MAX_FROM_8 (rgb  & 0xff);

      render = art_render_new (0, 0, style->width, style->height,
			       gdk_pixbuf_get_pixels (default_style->blank),
			       gdk_pixbuf_get_rowstride (default_style->blank),
			       gdk_pixbuf_get_n_channels (default_style->blank) - 1,
			       gdk_pixbuf_get_bits_per_sample (default_style->blank),
			       ART_ALPHA_PREMUL,
			       NULL);
      art_render_image_solid (render, color);
      art_render_svp (render, default_style->fill_svp);
      art_render_invoke (render);

      rgb = 0x000000;
      color[0] = ART_PIX_MAX_FROM_8 (rgb >> 16);
      color[1] = ART_PIX_MAX_FROM_8 ((rgb >> 8) & 0xff);
      color[2] = ART_PIX_MAX_FROM_8 (rgb  & 0xff);
      
      render = art_render_new (0, 0, style->width, style->height,
			       gdk_pixbuf_get_pixels (default_style->blank),
			       gdk_pixbuf_get_rowstride (default_style->blank),
			       gdk_pixbuf_get_n_channels (default_style->blank) - 1,
			       gdk_pixbuf_get_bits_per_sample (default_style->blank),
			       ART_ALPHA_PREMUL,
			       NULL);
      art_render_image_solid (render, color);
      art_render_svp (render, default_style->outline_svp);
      art_render_invoke (render);
    }

  gdk_pixbuf_save (default_style->blank, "foo.png", "png", NULL, NULL, NULL);
  return default_style->blank;
}

static void
gdeck_real_style_default_render_slot (GDeckStyle      *style,
				      GdkPixbuf       *pixbuf,
				      GDeckSlotType    type,
				      GDeckSlotLayout  layout,
				      GDeckState       state)
{
  GDeckStyleDefault *default_style = GDECK_STYLE_DEFAULT (style);
  ArtSVP *svp;
  guint color;
  if (type == GDECK_SLOT_TYPE_INVISIBLE)
    return;

  art_rgb_svp_alpha (default_style->fill_svp,
		     0, 0, style->width, style->height,
		     default_style->slot_fill_color,
		     gdk_pixbuf_get_pixels (pixbuf),
		     gdk_pixbuf_get_rowstride (pixbuf),
		     NULL);
  art_rgb_svp_alpha (default_style->outline_svp,
		     0, 0, style->width, style->height,
		     default_style->outline_color,
		     gdk_pixbuf_get_pixels (pixbuf),
		     gdk_pixbuf_get_rowstride (pixbuf),
		     NULL);

  switch (type)
    {
    case GDECK_SLOT_TYPE_STOCK:
      color = default_style->slot_highlight_color;
      svp = default_style->stock_svp;
      break;
    case GDECK_SLOT_TYPE_STOCK_USED:
      color = default_style->slot_stock_used_color;
      svp = default_style->stock_svp;
      break;
    case GDECK_SLOT_TYPE_WASTE:
      color = default_style->slot_highlight_color;
      svp = default_style->waste_svp;
      break;
    default:
      return;
    }

  art_rgb_svp_alpha (svp, 0, 0,
		     style->width, style->height, color,
		     gdk_pixbuf_get_pixels (pixbuf),
		     gdk_pixbuf_get_rowstride (pixbuf),
		     NULL);
}


static void
gdeck_real_style_default_render_card (GDeckStyle      *style,
				      GdkPixbuf       *pixbuf,
				      GDeckCard       *card,
				      gint             x,
				      gint             y,
				      GDeckSlotLayout  layout,
				      GDeckState       state,
				      gboolean         next_card)
{
  GDeckStyleDefault *default_style = GDECK_STYLE_DEFAULT (style);
  gint width, height;
  gint radius = GDECK_STYLE_DEFAULT (style)->radius;

  if (next_card)
    {
      switch (layout)
	{
	case GDECK_SLOT_CENTER:
	  width = style->width;
	  height = style->height;
	  break;
	case GDECK_SLOT_RIGHT:
	case GDECK_SLOT_LEFT:
	  height = style->height;
	  if (card->face_up)
	    width = style->hslot_offset + radius;
	  else
	    width = style->hslot_hidden_offset + radius;
	  break;
	case GDECK_SLOT_DOWN:
	case GDECK_SLOT_UP:
	  width = style->width;
	  if (card->face_up)
	    height = style->vslot_offset + radius;
	  else
	    height = style->vslot_hidden_offset + radius;
	  break;
	default:
	  g_assert_not_reached ();
	  return;
	}
    }
  else
    {
      width = style->width;
      height = style->height;
    }

  if (gdk_pixbuf_get_has_alpha (pixbuf))
    {
      ArtRender *render;
      guint rgb;
      ArtPixMaxDepth color[4];

      rgb = 0xffffff;
      color[0] = ART_PIX_MAX_FROM_8 (rgb >> 16);
      color[1] = ART_PIX_MAX_FROM_8 ((rgb >> 8) & 0xff);
      color[2] = ART_PIX_MAX_FROM_8 (rgb  & 0xff);

      render = art_render_new (-x, -y, width, height,
			       gdk_pixbuf_get_pixels (pixbuf),
			       gdk_pixbuf_get_rowstride (pixbuf),
			       gdk_pixbuf_get_n_channels (pixbuf) - 1,
			       gdk_pixbuf_get_bits_per_sample (pixbuf),
			       ART_ALPHA_PREMUL,
			       NULL);
      art_render_image_solid (render, color);
      art_render_svp (render, default_style->fill_svp);
      art_render_invoke (render);

      rgb = 0x000000;
      color[0] = ART_PIX_MAX_FROM_8 (rgb >> 16);
      color[1] = ART_PIX_MAX_FROM_8 ((rgb >> 8) & 0xff);
      color[2] = ART_PIX_MAX_FROM_8 (rgb  & 0xff);
      
      render = art_render_new (-x, -y, width, height,
			       gdk_pixbuf_get_pixels (pixbuf),
			       gdk_pixbuf_get_rowstride (pixbuf),
			       gdk_pixbuf_get_n_channels (pixbuf) - 1,
			       gdk_pixbuf_get_bits_per_sample (pixbuf),
			       ART_ALPHA_PREMUL,
			       NULL);
      art_render_image_solid (render, color);
      art_render_svp (render, default_style->outline_svp);
      art_render_invoke (render);
    }
  else
    {
      art_rgb_svp_alpha (default_style->fill_svp,
			 - x, - y,
			 width, height,
			 default_style->card_fill_color,
			 gdk_pixbuf_get_pixels (pixbuf),
			 gdk_pixbuf_get_rowstride (pixbuf),
			 NULL);
      art_rgb_svp_alpha (default_style->outline_svp,
			 - x,  - y,
			 width, height,
			 default_style->outline_color,
			 gdk_pixbuf_get_pixels (pixbuf),
			 gdk_pixbuf_get_rowstride (pixbuf),
			 NULL);
    }

  /* Back of card */
  if (! card->face_up)
    {
      gint x_offset, y_offset;

      x_offset = (style->width - gdk_pixbuf_get_width (default_style->bg_pixbuf))/2;
      y_offset = (style->height - gdk_pixbuf_get_height (default_style->bg_pixbuf))/2;

      gdk_pixbuf_copy_area (default_style->bg_pixbuf,
			    0, 0,
			    gdk_pixbuf_get_width (default_style->bg_pixbuf),
			    gdk_pixbuf_get_height (default_style->bg_pixbuf),
			    pixbuf,
			    x + x_offset,
			    y + y_offset);
    }
}
