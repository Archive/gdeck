/*
 * gdeckboard.c
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gdeckboard.h"
#include <gtk/gtklayout.h>
#include <gtk/gtksignal.h>
#include "gdeckmarshal.h"

enum
{
  CARDS_DROPPED,
  LAST_SIGNAL
};

typedef struct
{
  GDeckSlot *slot;
  gint x;
  gint y;
  gint x_offset;
  gint y_offset;
  gfloat x_position;
  gfloat y_position;
} SlotInfo;

static guint board_signals [LAST_SIGNAL] = { 0 };

/* type methods */
static void     gdeck_board_init                 (GDeckBoard      *board);
static void     gdeck_board_class_init           (GDeckBoardClass *class);

/* gobject methods */
static void     gdeck_board_finalize             (GObject         *object);

/* object methods */
static void     gdeck_board_destroy              (GtkObject       *object);

/* widget methods */
static gint     gdeck_board_button_release_event (GtkWidget       *widget,
						  GdkEventButton  *event);
static gint     gdeck_board_motion_notify_event  (GtkWidget       *widget,
						  GdkEventMotion  *event);
static gint     gdeck_board_expose_event         (GtkWidget       *widget,
						  GdkEventExpose  *event);
static void     gdeck_board_size_request         (GtkWidget       *widget,
						  GtkRequisition  *requisition);
static void     gdeck_board_realize              (GtkWidget       *widget);


/* internal function */
static void     gdeck_board_real_set_back_image  (GDeckBoard      *board);
static void     gdeck_board_get_slot_info        (SlotInfo        *slot_info);
static void     gdeck_board_handle_release_event (GDeckBoard      *board,
						  GdkEventButton  *event);



static GtkLayoutClass *parent_class = NULL;


GType
gdeck_board_get_type (void)
{
  static GType board_type = 0;

  if (! board_type)
    {
      static const GTypeInfo board_info =
      {
	sizeof (GDeckBoardClass),
	NULL,           /* base_init */
	NULL,           /* base_finalize */
	(GClassInitFunc) gdeck_board_class_init,
	NULL,           /* class_finalize */
	NULL,           /* class_data */
	sizeof (GDeckBoard),
	0,             /* n_preallocs */
	(GInstanceInitFunc) gdeck_board_init,
      };

      board_type = g_type_register_static (GTK_TYPE_LAYOUT, "GDeckBoard", &board_info, 0);
    }

  return board_type;
}

GtkWidget *
gdeck_board_new (GDeckStyle    *style,
		 GtkAdjustment *hadjustment,
		 GtkAdjustment *vadjustment)
{
  GtkWidget *retval;

  g_return_val_if_fail (GDECK_IS_STYLE (style), NULL);

  retval = GTK_WIDGET (gtk_type_new (GDECK_TYPE_BOARD));

  g_object_ref (style);
  GDECK_BOARD (retval)->style = style;

  gtk_layout_set_hadjustment (GTK_LAYOUT (retval), hadjustment);
  gtk_layout_set_vadjustment (GTK_LAYOUT (retval), vadjustment);

  return retval;
}

/* type methods */
static void
gdeck_board_init (GDeckBoard *board)
{
  gtk_widget_set_app_paintable (GTK_WIDGET (board), TRUE);

  board->slots = NULL;
  board->x_offset = 0;
  board->y_offset = 0;
  board->event = GDECK_EVENT_NONE;
}

static void
gdeck_board_class_init (GDeckBoardClass *class)
{
  GObjectClass *gobject_class = (GObjectClass *) class;
  GtkObjectClass *object_class = (GtkObjectClass *) class;
  GtkWidgetClass *widget_class = (GtkWidgetClass *) class;

  parent_class = gtk_type_class (GTK_TYPE_LAYOUT);

  gobject_class->finalize = gdeck_board_finalize;

  object_class->destroy = gdeck_board_destroy;

  widget_class->button_release_event = gdeck_board_button_release_event;
  widget_class->motion_notify_event = gdeck_board_motion_notify_event;
  widget_class->expose_event = gdeck_board_expose_event;
  widget_class->size_request = gdeck_board_size_request;
  widget_class->realize = gdeck_board_realize;

  board_signals [CARDS_DROPPED] =
    gtk_signal_new ("cards_dropped",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GDeckBoardClass, cards_dropped),
		    gdeck_marshal_VOID__OBJECT_OBJECT_POINTER,
		    G_TYPE_NONE, 3,
		    GDECK_TYPE_SLOT,
		    GDECK_TYPE_SLOT,
		    G_TYPE_POINTER);
}

/* gobject methods */
static void
gdeck_board_finalize (GObject *object)
{
  GDeckBoard *board = (GDeckBoard *) object;

  if (board->bg_pixbuf)
    g_object_unref (board->bg_pixbuf);

  (G_OBJECT_CLASS (parent_class)->finalize) (object);
}

/* object methods */
static void
gdeck_board_destroy (GtkObject *object)
{
  GDeckBoard *board = (GDeckBoard *) object;

  if (board->style)
    {
      g_object_unref (board->style);
      board->style = NULL;
    }

  (GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

/* widget methods */

static gint
gdeck_board_expose_event (GtkWidget      *widget,
			  GdkEventExpose *event)
{
  if (event->window != GDECK_BOARD (widget)->card_window)
    return (* GTK_WIDGET_CLASS (parent_class)->expose_event) (widget, event);

  return FALSE;
}

static void
gdeck_board_size_request (GtkWidget      *widget,
			  GtkRequisition *requisition)
{
  GDeckBoard *board = (GDeckBoard *) widget;
  GList *list;
  gint left = 10000; /* Something ridiculously big for the Min below */
  gint right = -1;
  gint top = 10000;
  gint bottom = -1;

  (* GTK_WIDGET_CLASS (parent_class)->size_request) (widget, requisition);

  if (board->slots == NULL)
    return;

  for (list = board->slots; list; list = list->next)
    {
      SlotInfo *slot_info = (SlotInfo *) list->data;

      gdeck_board_get_slot_info (slot_info);

      left = MIN (left, slot_info->x);
      top = MIN (top, slot_info->y);
      right = MAX (right, slot_info->x + GTK_WIDGET (slot_info->slot)->requisition.width);
      bottom = MAX (bottom, slot_info->y + GTK_WIDGET (slot_info->slot)->requisition.height);
    }

  left = MAX (left, 0);
  top = MAX (top, 0);

  /* Center the slots */
  requisition->width = right + left;
  requisition->height = top + bottom;
  gtk_layout_set_size (GTK_LAYOUT (widget), requisition->width, requisition->height);
}

static void
gdeck_board_size_allocate (GtkWidget     *widget,
			   GtkAllocation *allocation)
{
  gtk_layout_set_size (GTK_LAYOUT (widget), allocation->width, allocation->height);
  (GTK_WIDGET_CLASS (parent_class)->size_allocate) (widget, allocation);
}

static void
gdeck_board_realize (GtkWidget *widget)
{
  GDeckBoard *board = (GDeckBoard *) widget;
  GdkWindowAttr attributes;

  (GTK_WIDGET_CLASS (parent_class)->realize) (widget);

  if (board->bg_pixbuf)
    gdeck_board_real_set_back_image (board);

  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.event_mask = GDK_EXPOSURE_MASK;
  attributes.width = 1;
  attributes.height = 1;
  attributes.colormap = gdk_window_get_colormap (GTK_LAYOUT (widget)->bin_window);
  attributes.visual = gdk_window_get_visual (GTK_LAYOUT (widget)->bin_window);

  board->card_window = gdk_window_new(GTK_LAYOUT (widget)->bin_window,
				      &attributes,
				      (GDK_WA_VISUAL | GDK_WA_COLORMAP));
  gdk_window_set_user_data (board->card_window, widget);
}

static gboolean
gdeck_board_point_in_slot (GtkWidget *widget,
			   gint       x,
			   gint       y)
{
  return ((x >= widget->allocation.x) &&
	  (y >= widget->allocation.y) &&
	  (x <= widget->allocation.x + widget->allocation.width) &&
	  (y <= widget->allocation.y + widget->allocation.height));
}

static void
gdeck_board_handle_release_event (GDeckBoard     *board,
				  GdkEventButton *event)
{
  GList *list;
  GDeckBoardDropType drop_type = GDECK_BOARD_DROP_REJECT;
  GDeckSlot *drop_slot = NULL;

  gdk_window_hide (board->card_window);
  g_object_unref (G_OBJECT (board->event_pixbuf));

  for (list = board->slots; list; list = list->next)
    {
      SlotInfo *slot_info = (SlotInfo *) list->data;

      if (gdeck_board_point_in_slot (GTK_WIDGET (slot_info->slot), event->x, event->y))
	{
	  drop_slot = slot_info->slot;

	  if (board->drop_func)
	    drop_type = (* board->drop_func) (board,
					      GDECK_SLOT (board->event_slot),
					      drop_slot,
					      board->event_cards,
					      board->drop_data);
	  break;
	}
    }
  if (drop_type == GDECK_BOARD_DROP_ACCEPT)
    gdeck_slot_add_cards (drop_slot, board->event_cards);
  else if (drop_type == GDECK_BOARD_DROP_REJECT)
    gdeck_slot_add_cards (GDECK_SLOT (board->event_slot), board->event_cards);

  gtk_signal_emit (GTK_OBJECT (board),
		   board_signals [CARDS_DROPPED],
		   GDECK_SLOT (board->event_slot),
		   drop_slot,
		   board->event_cards,
		   drop_type);

  if (drop_type == GDECK_BOARD_DROP_DISPOSE)
    {
      g_list_foreach (board->event_cards, (GFunc) gdeck_card_free, NULL);
      g_list_free (board->event_cards);
    }
}


static gint
gdeck_board_button_release_event (GtkWidget      *widget,
				  GdkEventButton *event)
{
  GDeckBoard *board = (GDeckBoard *) widget;

  switch (board->event)
    {
    case GDECK_EVENT_NONE:
      return FALSE;
    case GDECK_EVENT_MOVING:
      if (event->button != board->style->drag_button)
	return FALSE;
      gdeck_board_handle_release_event (board, event);
      break;
    default:
      g_assert_not_reached ();
      return FALSE;
    }

  board->event_slot = NULL;
  board->event_cards = NULL;
  board->event_pixbuf = NULL;
  gdk_pointer_ungrab (event->time);
  board->event = GDECK_EVENT_NONE;
  return FALSE;
}

static gint
gdeck_board_motion_notify_event (GtkWidget      *widget,
				 GdkEventMotion *event)
{
  GDeckBoard *board = (GDeckBoard *) widget;

  if (board->event != GDECK_EVENT_MOVING)
    return FALSE;

  if (event->window == (GTK_LAYOUT (board)->bin_window))
    {
      gdk_window_move (board->card_window,
		       ((gint) event->x) + board->x_offset,
		       ((gint) event->y) + board->y_offset);
    }
  return FALSE;
}

static void
gdeck_board_real_set_back_image (GDeckBoard *board)
{
  GdkPixmap *pixmap;
  GdkWindow *window = GTK_LAYOUT (board)->bin_window;
  GdkPixbuf *pixbuf = board->bg_pixbuf;
  GdkGC *gc;

  g_assert (window != NULL);
  g_assert (pixbuf != NULL);

  pixmap = gdk_pixmap_new (window,
			   gdk_pixbuf_get_width (pixbuf),
			   gdk_pixbuf_get_height (pixbuf),
			   gdk_drawable_get_depth (window));

  gc = gdk_gc_new (pixmap);

  gdk_pixbuf_render_to_drawable (pixbuf,
				 pixmap,
				 gc, 0, 0, 0, 0,
				 gdk_pixbuf_get_width (pixbuf),
				 gdk_pixbuf_get_height (pixbuf),
				 GDK_RGB_DITHER_MAX,
				 0, 0);
  gdk_gc_unref (gc);

  gdk_window_set_back_pixmap (window, pixmap, FALSE);
  gdk_pixmap_unref (pixmap);
}

static void
gdeck_board_get_slot_info (SlotInfo *slot_info)
{
  gdeck_slot_request_offset (slot_info->slot, &slot_info->x_offset, &slot_info->y_offset);
  slot_info->x = slot_info->x_offset + (gint) (slot_info->x_position * slot_info->slot->style->width);
  slot_info->y = slot_info->y_offset + (gint) (slot_info->y_position * slot_info->slot->style->height);
}

/* Public API
 */
void
gdeck_board_set_style (GDeckBoard *board,
		       GDeckStyle *style)
{
  GList *list;

  g_return_if_fail (GDECK_IS_BOARD (board));
  g_return_if_fail (GDECK_IS_STYLE (style));

  g_object_ref (style);
  g_object_unref (board->style);

  board->style = style;

  for (list = board->slots; list; list = list->next)
    gdeck_slot_set_style (((SlotInfo *) list->data)->slot, style);

  if (GTK_WIDGET_REALIZED (board))
    gtk_widget_queue_resize (GTK_WIDGET (board));
}

void
gdeck_board_set_back_image (GDeckBoard *board,
			    GdkPixbuf  *image)
{
  GList *list;

  g_return_if_fail (GDECK_IS_BOARD (board));
  g_return_if_fail (GDK_IS_PIXBUF (image));
  if (gdk_pixbuf_get_has_alpha (image))
    {
      g_warning ("Background images with alpha channels are not supported yet.\n");
      return;
    }

  if (image)
    {
      g_object_ref (image);
      board->color_set = FALSE;
    }

  if (board->bg_pixbuf)
    g_object_unref (board->bg_pixbuf);

  board->bg_pixbuf = image;

  if (GTK_WIDGET_REALIZED (board) && board->bg_pixbuf)
    gdeck_board_real_set_back_image (board);

  for (list = board->slots; list; list = list->next)
    gdeck_slot_set_back_image (((SlotInfo *)list->data)->slot, image);
}

static void
gdeck_board_drag_cards (GDeckSlot              *slot,
			GList                  *cards,
			gint                    x_offset,
			gint                    y_offset,
			GDeckBoard             *board)
{
  if (gdk_pointer_grab (GTK_LAYOUT (board)->bin_window, FALSE,
			GDK_BUTTON_RELEASE_MASK |
			GDK_POINTER_MOTION_MASK,
			NULL, NULL,
			GDK_CURRENT_TIME) == 0)
    {
      GdkPixmap *event_pixmap;
      GdkBitmap *event_mask;
      gint x, y;

      gdeck_slot_remove_cards (slot, cards);

      board->event = GDECK_EVENT_MOVING;
      board->event_slot = GTK_WIDGET (slot);
      board->event_cards = cards;
      board->x_offset = x_offset;
      board->y_offset = y_offset;
      board->event_pixbuf = NULL;

      gdk_window_get_pointer (GTK_LAYOUT (board)->bin_window,
			      &x, &y, NULL);
      gdeck_style_render_cards (board->style, slot->layout,
				&board->event_pixbuf, cards);
      gdk_window_move_resize (board->card_window,
			      x + board->x_offset,
			      y + board->y_offset,
			      gdk_pixbuf_get_width (board->event_pixbuf),
			      gdk_pixbuf_get_height (board->event_pixbuf));

      gdk_pixbuf_render_pixmap_and_mask (board->event_pixbuf,
					 &event_pixmap,
					 &event_mask,
					 1);

      gdk_window_set_back_pixmap (board->card_window, event_pixmap, FALSE);
      gdk_window_shape_combine_mask (board->card_window, event_mask, 0, 0);
      gdk_window_show (board->card_window);
    }
}

void
gdeck_board_add_slot (GDeckBoard *board,
		      GDeckSlot  *slot,
		      gdouble     x_position,
		      gdouble     y_position)
{
  SlotInfo *slot_info;

  g_return_if_fail (GDECK_IS_BOARD (board));
  g_return_if_fail (GDECK_IS_SLOT (slot));
  g_return_if_fail (x_position >= 0.0 && y_position >= 0.0);

  gdeck_slot_set_style (slot, board->style);

  slot_info = g_new0 (SlotInfo, 1);
  slot_info->slot = slot;
  slot_info->x_position = x_position;
  slot_info->y_position = y_position;
  gdeck_board_get_slot_info (slot_info);

  gtk_signal_connect (GTK_OBJECT (slot),
		      "drag_cards",
		      GTK_SIGNAL_FUNC (gdeck_board_drag_cards),
		      board);

  board->slots = g_list_append (board->slots, slot_info);
  gtk_layout_put (GTK_LAYOUT (board), GTK_WIDGET (slot),
		  slot_info->x, slot_info->y);

  if (board->bg_pixbuf)
    gdeck_slot_set_back_image (slot, board->bg_pixbuf);
  else if (board->color_set)
    gdeck_slot_set_background (slot, &(board->bg_color));
}

GList *
gdeck_board_get_slots (GDeckBoard *board)
{
  GList *list;
  GList *retval = NULL;

  g_return_val_if_fail (GDECK_IS_BOARD (board), NULL);

  for (list = board->slots; list; list = list->next)
    retval = g_list_append (retval, ((SlotInfo *)list->data)->slot);

  return retval;
}


void
gdeck_board_start_move (GDeckBoard *board,
			GDeckSlot  *from_slot,
			GDeckSlot  *to_slot,
			GList      *cards,
			gboolean    flip)
{
  g_return_if_fail (GDECK_IS_BOARD (board));
  g_return_if_fail (GDECK_IS_SLOT (from_slot));
  g_return_if_fail (GDECK_IS_SLOT (to_slot));
  g_return_if_fail (board->event != GDECK_EVENT_NONE);

  board->event = GDECK_EVENT_ANIMATION;
}

void
gdeck_board_set_drop_func (GDeckBoard         *board,
			   GDeckBoardDropFunc  drop_func,
			   gpointer            drop_data,
			   GtkDestroyNotify    drop_destroy)
{
  g_return_if_fail (GDECK_IS_BOARD (board));

  if (board->drop_destroy)
    (* board->drop_destroy) (board->drop_data);

  board->drop_func = drop_func;
  board->drop_data = drop_data;
  board->drop_destroy = drop_destroy;
}
