/*
 * gdeckboard.h
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_BOARD_H__
#define __GDECK_BOARD_H__

#include <gtk/gtklayout.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "gdeckslot.h"

#define GDECK_TYPE_BOARD            (gdeck_board_get_type ())
#define GDECK_BOARD(obj)            (GTK_CHECK_CAST ((obj), GDECK_TYPE_BOARD, GDeckBoard))
#define GDECK_BOARD_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GDECK_TYPE_BOARD, GDeckBoardClass))
#define GDECK_IS_BOARD(obj)         (GTK_CHECK_TYPE ((obj), GDECK_TYPE_BOARD))
#define GDECK_IS_BOARD_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GDECK_TYPE_BOARD))


typedef struct _GDeckBoard GDeckBoard;
typedef struct _GDeckBoardClass GDeckBoardClass;

typedef enum {
  GDECK_EVENT_NONE,
  GDECK_EVENT_ANIMATION,
  GDECK_EVENT_MOVING,
} GDeckEvent;

typedef enum {
  GDECK_BOARD_DROP_REJECT,
  GDECK_BOARD_DROP_DISPOSE,
  GDECK_BOARD_DROP_ACCEPT,
} GDeckBoardDropType;

typedef GDeckBoardDropType (* GDeckBoardDropFunc) (GDeckBoard *board,
						   GDeckSlot  *start_slot,
						   GDeckSlot  *end_slot,
						   GList      *cards,
						   gpointer    data);

struct _GDeckBoard
{
  GtkLayout parent;

  GList *slots;
  GDeckStyle *style;
  GdkPixbuf *bg_pixbuf;
  GdkColor bg_color;

  GDeckBoardDropFunc drop_func;
  gpointer drop_data;
  GtkDestroyNotify drop_destroy;
  
  /* Event info */
  GDeckEvent event;
  GdkWindow *card_window;
  GtkWidget *event_slot;
  GdkPixbuf *event_pixbuf;
  GList *event_cards;
  gint x_offset;
  gint y_offset;

  /* settings */
  gint drag_button;
  gint move_button;
  gint reveal_button;

  /* Flags */
  guint color_set : 1;
};

struct _GDeckBoardClass {
  GtkLayoutClass parent_class;

  void (* cards_dropped) (GDeckBoard         *board,
			  GDeckSlot          *start_slot,
			  GDeckSlot          *end_slot,
			  GList              *cards,
			  GDeckBoardDropType  type);
};


GType      gdeck_board_get_type       (void);
GtkWidget *gdeck_board_new            (GDeckStyle         *style,
				       GtkAdjustment      *hadjustment,
				       GtkAdjustment      *vadjustment);
void       gdeck_board_set_style      (GDeckBoard         *board,
				       GDeckStyle         *style);
void       gdeck_board_set_back_image (GDeckBoard         *board,
				       GdkPixbuf          *background);
void       gdeck_board_set_background (GDeckBoard         *board,
				       GdkColor           *color);
void       gdeck_board_add_slot       (GDeckBoard         *board,
				       GDeckSlot          *slot,
				       gdouble             x_offset,
				       gdouble             y_offset);
GList     *gdeck_board_get_slots      (GDeckBoard         *board);
void       gdeck_board_start_move     (GDeckBoard         *board,
				       GDeckSlot          *from_slot,
				       GDeckSlot          *to_slot,
				       GList              *cards,
				       gboolean            flip);
void       gdeck_board_set_drop_func  (GDeckBoard         *board,
				       GDeckBoardDropFunc  drop_func,
				       gpointer            drop_data,
				       GtkDestroyNotify    drop_destroy);

#endif

