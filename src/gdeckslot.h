/*
 * gdeckslot.h
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_SLOT_H__
#define __GDECK_SLOT_H__

#include <gtk/gtklayout.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "gdeckstyle.h"
#include "gdeckcard.h"

#define GDECK_TYPE_SLOT            (gdeck_slot_get_type ())
#define GDECK_SLOT(obj)            (GTK_CHECK_CAST ((obj), GDECK_TYPE_SLOT, GDeckSlot))
#define GDECK_SLOT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GDECK_TYPE_SLOT, GDeckSlotClass))
#define GDECK_IS_SLOT(obj)         (GTK_CHECK_TYPE ((obj), GDECK_TYPE_SLOT))
#define GDECK_IS_SLOT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GDECK_TYPE_SLOT))


typedef struct _GDeckSlot GDeckSlot;
typedef struct _GDeckSlotClass GDeckSlotClass;

typedef gboolean (* GDeckSlotDragFunc) (GDeckSlot *slot,
					GList     *cards,
					gpointer   data);


struct _GDeckSlot
{
  GtkWidget parent;

  GDeckSlotType type;
  GDeckSlotLayout layout;
  GDeckState state;
  GDeckStyle *style;
  GList *cards;
  gint partial_count;
  GDeckSlotDragFunc func;
  gpointer data;
  GtkDestroyNotify destroy;

  GdkPixbuf *bg_pixbuf;
  GdkColor bg_color;

  GdkPixbuf *reveal_pixbuf;
  gint reveal_offset;
  gint x_offset;
  gint y_offset;

  guint dirty : 1;
  guint color_set : 1;
  guint partial : 1;
  guint fill_partial : 1;
  guint maybe_move : 1;
  guint maybe_activate : 1;
  guint in_reveal : 1;
};

struct _GDeckSlotClass
{
  GtkWidgetClass parent_class;

  void (* drag_cards)     (GDeckSlot              *slot,
			   GList                  *cards,
			   gint                    x_offset,
			   gint                    y_offset);
  void (* card_selected)  (GDeckSlot              *slot,
			   GDeckCard              *card);
  void (* card_activated) (GDeckSlot              *slot,
			   GDeckCard              *card);
};

GtkType    gdeck_slot_get_type       (void);
GtkWidget *gdeck_slot_new            (GDeckSlotType      type,
				      GDeckSlotLayout    layout);
void       gdeck_slot_set_drag_func  (GDeckSlot         *slot,
				      GDeckSlotDragFunc  func,
				      gpointer           data,
				      GtkDestroyNotify   destroy);
void       gdeck_slot_set_style      (GDeckSlot         *slot,
				      GDeckStyle        *style);
void       gdeck_slot_set_type       (GDeckSlot         *slot,
				      GDeckSlotType      type);
void       gdeck_slot_request_offset (GDeckSlot         *slot,
				      gint              *x_offset,
				      gint              *y_offset);
void       gdeck_slot_set_back_image (GDeckSlot         *slot,
				      GdkPixbuf         *background);
void       gdeck_slot_set_background (GDeckSlot         *slot,
				      GdkColor          *color);
GList     *gdeck_slot_get_cards      (GDeckSlot         *slot);
void       gdeck_slot_add_cards      (GDeckSlot         *slot,
				      GList             *cards);
void       gdeck_slot_remove_cards   (GDeckSlot         *slot,
				      GList             *cards);


#endif
