#ifndef __GDK_PIXBUF_ROTATE_H__
#define __GDK_PIXBUF_ROTATE_H__

#include <gdk-pixbuf/gdk-pixbuf.h>

GdkPixbuf *gdk_pixbuf_rotate_0   (GdkPixbuf *pixbuf);
GdkPixbuf *gdk_pixbuf_rotate_90  (GdkPixbuf *pixbuf);
GdkPixbuf *gdk_pixbuf_rotate_180 (GdkPixbuf *pixbuf);
GdkPixbuf *gdk_pixbuf_rotate_270 (GdkPixbuf *pixbuf);
GdkPixbuf *gdk_pixbuf_rotate     (GdkPixbuf *pixbuf,
				  gdouble    angle);

#endif
