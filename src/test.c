#include "gdeck.h"
#include <gtk/gtk.h>
#include <sys/time.h>

int
main (int argc, char *argv [])
{
  GtkWidget *window;
  GtkWidget *board;
  GtkWidget *slot;
  GdkPixbuf *pixbuf;
  GDeckStyle *style;
  GList *deck;
  struct timeval time_a;
  struct timeval time_b;

  gtk_init (&argc, &argv);
  gdeck_init ();

  deck = gdeck_deck_new (TRUE);

  style = gdeck_style_fc_new ();

  gettimeofday (&time_a, NULL);
  gdeck_style_generate_cards (style, NULL);
  gettimeofday (&time_b, NULL);

  if (time_b.tv_usec < time_a.tv_usec)
    g_print ("%ld.%6ld\n", time_b.tv_sec - (time_a.tv_sec + 1), 1000000+(time_b.tv_usec - time_a.tv_usec));
  else
    g_print ("%ld.%6ld\n", time_b.tv_sec - time_a.tv_sec, time_b.tv_usec - time_a.tv_usec);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  board = gdeck_board_new (style, NULL, NULL);
  g_object_unref (style);

  if (argc == 2)
    {
      pixbuf = gdk_pixbuf_new_from_file (argv[1], NULL);
      gdeck_board_set_back_image (GDECK_BOARD (board), pixbuf);
    }
  gtk_container_add (GTK_CONTAINER (window), board);

  slot = gdeck_slot_new (GDECK_SLOT_TYPE_NORMAL, GDECK_SLOT_CENTER);
  gdeck_board_add_slot (GDECK_BOARD (board), GDECK_SLOT (slot), 0.1, 0.1);

  slot = gdeck_slot_new (GDECK_SLOT_TYPE_INVISIBLE, GDECK_SLOT_CENTER);
  gdeck_board_add_slot (GDECK_BOARD (board), GDECK_SLOT (slot), 1.2, 0.1);

  slot = gdeck_slot_new (GDECK_SLOT_TYPE_STOCK, GDECK_SLOT_CENTER);
  gdeck_board_add_slot (GDECK_BOARD (board), GDECK_SLOT (slot), 2.3, 0.1);

  slot = gdeck_slot_new (GDECK_SLOT_TYPE_STOCK_USED, GDECK_SLOT_CENTER);
  gdeck_board_add_slot (GDECK_BOARD (board), GDECK_SLOT (slot), 3.4, 0.1);

  slot = gdeck_slot_new (GDECK_SLOT_TYPE_WASTE, GDECK_SLOT_CENTER);
  gdeck_board_add_slot (GDECK_BOARD (board), GDECK_SLOT (slot), 4.5, 0.1);

  slot = gdeck_slot_new (GDECK_SLOT_TYPE_NORMAL, GDECK_SLOT_DOWN);
  gdeck_board_add_slot (GDECK_BOARD (board), GDECK_SLOT (slot), 0.1, 1.2);
  gdeck_slot_add_cards (GDECK_SLOT (slot), deck);

  gtk_widget_show_all (window);
  gtk_main ();

  return 0;
}
