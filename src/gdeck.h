#ifndef __GDECK_H__
#define __GDECK_H__

#include "gdeckenums.h"
#include "gdeckboard.h"
#include "gdeckcard.h"
#include "gdeckslot.h"
#include "gdeckstyle.h"
#include "gdeckstyledefault.h"
#include "gdeckstylefc.h"

void gdeck_init (void);

#endif
