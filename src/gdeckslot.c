/*
 * gdeckslot.c
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>

#include "gdeckslot.h"
#include <gtk/gtkwidget.h>
#include <gtk/gtkdnd.h>
#include <gtk/gtksignal.h>
#include "gdeckstyle.h"
#include "gdeckstyledefault.h"
#include "gdeckmarshal.h"

enum
{
  DRAG_CARDS,
  CARD_SELECTED,
  CARD_ACTIVATED,
  LAST_SIGNAL
};


/* type methods */
static void       gdeck_slot_init                 (GDeckSlot       *slot);
static void       gdeck_slot_class_init           (GDeckSlotClass  *class);

/* gobject methods */
static void       gdeck_slot_finalize             (GObject         *object);

/* object methods */
static void       gdeck_slot_destroy              (GtkObject       *obkect);

/* widget methods */
static void       gdeck_slot_realize              (GtkWidget       *widget);
static void       gdeck_slot_size_request         (GtkWidget       *widget,
						   GtkRequisition  *requisition);
static void       gdeck_slot_size_allocate        (GtkWidget       *widget,
						   GtkAllocation   *allocation);
static gint       gdeck_slot_expose_event         (GtkWidget       *widget,
						   GdkEventExpose  *event);
static gint       gdeck_slot_motion_event         (GtkWidget       *widget,
						   GdkEventMotion  *event);
static gint       gdeck_slot_leave_notify_event   (GtkWidget       *widget,
						   GdkEventCrossing *event);
static gint       gdeck_slot_button_press_event   (GtkWidget       *widget,
						   GdkEventButton  *event);
static gint       gdeck_slot_button_release_event (GtkWidget       *widget,
						   GdkEventButton  *event);


/* helper functions */
static gboolean   cards_in_point_helper           (GDeckStyle      *style,
						   GDeckSlotLayout  layout,
						   GList           *card_list,
						   gint             x,
						   gint             y,
						   gint             width,
						   gint             height,
						   gpointer         data);
static GdkPixbuf *gdeck_slot_get_bg_image         (GDeckSlot       *slot);


static GtkWidgetClass *parent_class = NULL;
static guint slot_signals [LAST_SIGNAL] = { 0 };


typedef struct
{
  GList    *list;
  gint      x;
  gint      y;
  gboolean  point_in_cards;
  gint      x_offset;
  gint      y_offset;
} CardsInPointData;


/* type methods */
GtkType
gdeck_slot_get_type (void)
{
  static GtkType slot_type = 0;

  if (! slot_type)
    {
      static const GTypeInfo slot_info =
      {
	sizeof (GDeckSlotClass),
	NULL,           /* base_init */
	NULL,           /* base_finalize */
	(GClassInitFunc) gdeck_slot_class_init,
	NULL,           /* class_finalize */
	NULL,           /* class_data */
	sizeof (GDeckSlot),
	0,             /* n_preallocs */
	(GInstanceInitFunc) gdeck_slot_init,
      };

      slot_type = g_type_register_static (GTK_TYPE_WIDGET, "GDeckSlot", &slot_info, 0);
    }

  return slot_type;
}

GtkWidget *
gdeck_slot_new (GDeckSlotType   type,
		GDeckSlotLayout layout)
{
  GtkWidget *retval;

  g_return_val_if_fail (type >= 0 && type < GDECK_SLOT_NUM_TYPES, NULL);
  g_return_val_if_fail (layout >= 0 && layout < GDECK_SLOT_NUM_LAYOUTS, NULL);

  retval = GTK_WIDGET (gtk_type_new (GDECK_TYPE_SLOT));

  GDECK_SLOT (retval)->type = type;
  GDECK_SLOT (retval)->layout = layout;

  return retval;
}

static void
gdeck_slot_init (GDeckSlot *slot)
{
  slot->type = GDECK_SLOT_TYPE_NORMAL;
  slot->layout = GDECK_SLOT_CENTER;
  slot->state = GDECK_STATE_NORMAL;
  slot->cards = NULL;
  slot->style = NULL;

  slot->func = NULL;
  slot->data = NULL;
  slot->destroy = NULL;

  slot->dirty = TRUE;
  slot->color_set = FALSE;
  slot->partial = FALSE;
  slot->fill_partial = FALSE;
  slot->in_reveal = FALSE;
}

static void
gdeck_slot_class_init (GDeckSlotClass *class)
{
  GObjectClass *o_class = (GObjectClass *) class;
  GtkObjectClass *object_class = (GtkObjectClass *) class;
  GtkWidgetClass *widget_class = (GtkWidgetClass *) class;

  parent_class = gtk_type_class (GTK_TYPE_WIDGET);

  o_class->finalize = gdeck_slot_finalize;

  object_class->destroy = gdeck_slot_destroy;

  widget_class->realize = gdeck_slot_realize;
  widget_class->size_request = gdeck_slot_size_request;
  widget_class->size_allocate = gdeck_slot_size_allocate;
  widget_class->expose_event = gdeck_slot_expose_event;
  widget_class->motion_notify_event = gdeck_slot_motion_event;
  widget_class->leave_notify_event = gdeck_slot_leave_notify_event;
  widget_class->button_press_event = gdeck_slot_button_press_event;
  widget_class->button_release_event = gdeck_slot_button_release_event;

  slot_signals [DRAG_CARDS] =
    gtk_signal_new ("drag_cards",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GDeckSlotClass, drag_cards),
		    gdeck_marshal_VOID__POINTER_INT_INT,
		    G_TYPE_NONE, 3,
		    G_TYPE_POINTER,
		    G_TYPE_INT,
		    G_TYPE_INT);

  slot_signals [CARD_SELECTED] =
    gtk_signal_new ("card_selected",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GDeckSlotClass, card_selected),
		    gtk_marshal_VOID__BOXED,
		    G_TYPE_NONE, 1,
		    GDECK_TYPE_CARD);

  slot_signals [CARD_ACTIVATED] =
    gtk_signal_new ("card_activated",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GDeckSlotClass, card_activated),
		    gtk_marshal_VOID__BOXED,
		    G_TYPE_NONE, 1,
		    GDECK_TYPE_CARD);

}

/* gobject methods */
static void
gdeck_slot_finalize (GObject *object)
{
  GDeckSlot *slot = (GDeckSlot *) object;
  GList *list;

  for (list = slot->cards; list; list = list->next)
    gdeck_card_free (GDECK_CARD (list->data));

  g_list_free (slot->cards);

  (* G_OBJECT_CLASS (parent_class)->finalize) (object);
}

/* object methods */
static void
gdeck_slot_destroy (GtkObject *object)
{
  GDeckSlot *slot = (GDeckSlot *) object;

  if (slot->style)
    g_object_unref (slot->style);

  slot->style = NULL;

  (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

/* widget methods */
static void
gdeck_slot_realize (GtkWidget *widget)
{
  GdkWindowAttr attributes;
  gint attributes_mask;

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget);
  attributes.event_mask |= (GDK_EXPOSURE_MASK |
			    GDK_POINTER_MOTION_MASK |
			    GDK_BUTTON_PRESS_MASK |
			    GDK_BUTTON_RELEASE_MASK |
			    GDK_ENTER_NOTIFY_MASK |
			    GDK_LEAVE_NOTIFY_MASK);

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, widget);

  widget->style = gtk_style_attach (widget->style, widget->window);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);

  GDECK_SLOT (widget)->dirty = TRUE;
}

static void
gdeck_slot_size_request (GtkWidget      *widget,
			 GtkRequisition *requisition)
{
  GDeckSlot *slot = (GDeckSlot *) widget;

  requisition->width = slot->style->width;
  requisition->height = slot->style->height;

  if (slot->cards)
    gdeck_style_get_size (slot->style,
			  slot->layout,
			  slot->cards,
			  &(requisition->width),
			  &(requisition->height));


  widget->requisition.width = requisition->width;
  widget->requisition.height = requisition->height;
}

static void
gdeck_slot_size_allocate (GtkWidget     *widget,
			  GtkAllocation *allocation)
{
  widget->allocation = *allocation;

  if (GTK_WIDGET_REALIZED (widget))
    gdk_window_move_resize (widget->window,
			    allocation->x,
			    allocation->y,
			    allocation->width,
			    allocation->height);
}

static gint
gdeck_slot_expose_event (GtkWidget      *widget,
			 GdkEventExpose *event)
{
  GDeckSlot *slot = (GDeckSlot *) widget;
  GdkWindow *window;
  GdkPixbuf *pixbuf;
  GdkPixmap *pixmap;
  GdkGC *gc;

  window = GTK_WIDGET (slot)->window;

  if (slot->in_reveal)
    {
      gint x = 0;
      gint y = 0;

      switch (slot->layout)
	{
	case GDECK_SLOT_CENTER:
	  break;
	case GDECK_SLOT_RIGHT:
	case GDECK_SLOT_LEFT:
	  x = slot->reveal_offset;
	  break;
	case GDECK_SLOT_DOWN:
	case GDECK_SLOT_UP:
	  y = slot->reveal_offset;
	  break;
	default:
	  g_assert_not_reached ();
	  break;
	}

      gdk_pixbuf_render_to_drawable_alpha (slot->reveal_pixbuf,
					   window,
					   0, 0, x, y,
					   gdk_pixbuf_get_width (slot->reveal_pixbuf),
					   gdk_pixbuf_get_height (slot->reveal_pixbuf),
					   GDK_PIXBUF_ALPHA_BILEVEL,
					   127,
					   GDK_RGB_DITHER_MAX,
					   0, 0);
      return TRUE;
    }

  if (! slot->dirty)
    return TRUE;

  pixbuf = gdeck_slot_get_bg_image (slot);

  if (slot->cards == NULL)
    {
      gdeck_style_render_slot (slot->style,
			       pixbuf,
			       slot->type,
			       slot->layout,
			       slot->state);
    }
  else
    {
      gdeck_style_render_cards (slot->style,
				slot->layout,
				&(pixbuf),
				slot->cards);
    }
  pixmap = gdk_pixmap_new (window,
			   gdk_pixbuf_get_width (pixbuf),
			   gdk_pixbuf_get_height (pixbuf),
			   gdk_drawable_get_depth (window));
  gc = gdk_gc_new (pixmap);

  gdk_pixbuf_render_to_drawable (pixbuf,
				 pixmap,
				 gc, 0, 0, 0, 0,
				 gdk_pixbuf_get_width (pixbuf),
				 gdk_pixbuf_get_height (pixbuf),
				 GDK_RGB_DITHER_MAX,
				 0, 0);
  gdk_gc_unref (gc);

  gdk_window_set_back_pixmap (window, pixmap, FALSE);
  gtk_widget_queue_draw (GTK_WIDGET (slot));
  gdk_pixbuf_unref (pixbuf);
  gdk_pixmap_unref (pixmap);

  slot->dirty = FALSE;

  return FALSE;
}

static gint
gdeck_slot_motion_event (GtkWidget      *widget,
			 GdkEventMotion *event)
{
  GDeckSlot *slot = (GDeckSlot *) widget;
  CardsInPointData data;

  if (! slot->maybe_move)
    return FALSE;

  if (!gtk_drag_check_threshold (widget,
				 slot->x_offset, slot->y_offset,
				 event->x, event->y))
    return FALSE;

  gdk_pointer_ungrab (event->time);
  slot->maybe_move = FALSE;

  data.list = NULL;
  data.x = slot->x_offset;
  data.y = slot->y_offset;
  data.point_in_cards = FALSE;

  gdeck_style_map_cards (slot->style,
			 slot->layout,
			 slot->cards,
			 cards_in_point_helper,
			 &data);

  if (! data.point_in_cards)
    return FALSE;

  if ((slot->func) &&
      ! (* slot->func) (slot, data.list, slot->data))
    return TRUE;

  gtk_signal_emit (GTK_OBJECT (widget),
		   slot_signals [DRAG_CARDS],
		   data.list,
		   data.x_offset,
		   data.y_offset);
  return TRUE;
}

static gint
gdeck_slot_leave_notify_event (GtkWidget        *widget,
			       GdkEventCrossing *event)
{
  GDeckSlot *slot = (GDeckSlot *) widget;
  CardsInPointData data;

  if (! slot->maybe_move)
    return FALSE;

  slot->maybe_move = FALSE;

  data.list = NULL;
  data.x = slot->x_offset;
  data.y = slot->y_offset;
  data.point_in_cards = FALSE;

  gdeck_style_map_cards (slot->style,
			 slot->layout,
			 slot->cards,
			 cards_in_point_helper,
			 &data);

  if (! data.point_in_cards)
    return FALSE;

  if ((slot->func) &&
      ! (* slot->func) (slot, data.list, slot->data))
    return TRUE;

  gtk_signal_emit (GTK_OBJECT (widget),
		   slot_signals [DRAG_CARDS],
		   data.list,
		   data.x_offset,
		   data.y_offset);
  return TRUE;
}

static gint
gdeck_slot_button_press_event (GtkWidget      *widget,
			       GdkEventButton *event)
{
  GDeckSlot *slot = (GDeckSlot *) widget;
  CardsInPointData data;

  if ((event->type == GDK_BUTTON_PRESS) &&
      (slot->in_reveal || slot->maybe_move || slot->maybe_activate))
    return TRUE;

  data.list = NULL;
  data.x = event->x;
  data.y = event->y;
  data.point_in_cards = FALSE;

  gdeck_style_map_cards (slot->style,
			 slot->layout,
			 slot->cards,
			 cards_in_point_helper,
			 &data);

  if (event->type == GDK_2BUTTON_PRESS ||
      event->type == GDK_3BUTTON_PRESS)
    {
      if (event->button == slot->style->drag_button &&
	  slot->maybe_move)
	{
	  gdk_pointer_ungrab (event->time);
	  slot->maybe_move = FALSE;
	  gtk_signal_emit (GTK_OBJECT (widget),
			   slot_signals [CARD_ACTIVATED],
			   data.point_in_cards?data.list->data:NULL);
	}
      return TRUE;
    }

  if (! data.point_in_cards)
    {
      /* FIXME: later, check to make sure it's actually in the slot, and not
       * just in the bounding box.*/
    }

  if (gdk_pointer_grab (widget->window, FALSE,
			GDK_BUTTON_PRESS_MASK |
			GDK_BUTTON_MOTION_MASK |
			GDK_BUTTON_RELEASE_MASK,
			NULL, NULL, event->time) == 0)
    {
      slot->x_offset = event->x;
      slot->y_offset = event->y;

      if (event->button == slot->style->drag_button)
	slot->maybe_move = TRUE;
      else if (event->button == slot->style->activate_button)
	slot->maybe_activate = TRUE;
      else if (event->button == slot->style->reveal_button)
	{
	  slot->reveal_pixbuf =
	    gdk_pixbuf_new (GDK_COLORSPACE_RGB,
			    TRUE, 8,
			    slot->style->width,
			    slot->style->height);
	  switch (slot->layout)
	    {
	    case GDECK_SLOT_CENTER:
	      slot->reveal_offset = 0;
	      break;
	    case GDECK_SLOT_RIGHT:
	    case GDECK_SLOT_LEFT:
	      slot->reveal_offset = data.x + data.x_offset;
	      break;
	    case GDECK_SLOT_DOWN:
	    case GDECK_SLOT_UP:
	      slot->reveal_offset = data.y + data.y_offset;
	      break;
	    default:
	      g_assert_not_reached ();
	      break;
	    }
	  memset (gdk_pixbuf_get_pixels (slot->reveal_pixbuf),
		  0x00000000,
		  gdk_pixbuf_get_rowstride (slot->reveal_pixbuf) *
		  gdk_pixbuf_get_height (slot->reveal_pixbuf));
	  gdeck_style_render_card (slot->style,
				   slot->reveal_pixbuf,
				   GDECK_CARD (data.list->data),
				   0, 0,
				   slot->layout,
				   slot->state,
				   FALSE);
	  gtk_widget_queue_draw (GTK_WIDGET (slot));
	  slot->in_reveal = TRUE;
	}
    }

  return TRUE;
}

static gint
gdeck_slot_button_release_event (GtkWidget      *widget,
				 GdkEventButton *event)
{
  GDeckSlot *slot = (GDeckSlot *) widget;

  if (slot->in_reveal &&
      event->button == slot->style->reveal_button)
    {
      g_object_unref (slot->reveal_pixbuf);
      slot->reveal_pixbuf = NULL;
      gdk_pointer_ungrab (event->time);
      slot->in_reveal = FALSE;
      gtk_widget_queue_draw (widget);
    }
  else if (slot->maybe_move &&
	   event->button == slot->style->drag_button)
    {
      CardsInPointData data;

      gdk_pointer_ungrab (event->time);
      slot->maybe_move = FALSE;

      data.list = NULL;
      data.x = slot->x_offset;
      data.y = slot->y_offset;
      data.point_in_cards = FALSE;

      gdeck_style_map_cards (slot->style,
			     slot->layout,
			     slot->cards,
			     cards_in_point_helper,
			     &data);
      if (data.point_in_cards)
	{
	  gtk_signal_emit (GTK_OBJECT (widget),
			 slot_signals [CARD_SELECTED],
			 data.list->data);
	}
      else
	{
	  gtk_signal_emit (GTK_OBJECT (widget),
			   slot_signals [CARD_SELECTED],
			   NULL);
	}
    }
  else if (slot->maybe_activate &&
	   event->button == slot->style->activate_button)
    {
      CardsInPointData data;

      gdk_pointer_ungrab (event->time);
      slot->maybe_activate = FALSE;

      data.list = NULL;
      data.x = slot->x_offset;
      data.y = slot->y_offset;
      data.point_in_cards = FALSE;

      gdeck_style_map_cards (slot->style,
			     slot->layout,
			     slot->cards,
			     cards_in_point_helper,
			     &data);
      if (data.point_in_cards)
	{
	  gtk_signal_emit (GTK_OBJECT (widget),
			   slot_signals [CARD_ACTIVATED],
			   data.list->data);
	}
    }
  return TRUE;
}


/* Helper functions */

static gboolean
cards_in_point_helper (GDeckStyle      *style,
		       GDeckSlotLayout  layout,
		       GList           *list,
		       gint             x,
		       gint             y,
		       gint             width,
		       gint             height,
		       gpointer         data)
{
  CardsInPointData *cards_data = (CardsInPointData *) data;

  if ((cards_data->x >= x) &&
      (cards_data->y >= y) &&
      (cards_data->x <= x + width) &&
      (cards_data->y <= y + height))
    {
      cards_data->list = list;
      cards_data->x_offset = x - cards_data->x;
      cards_data->y_offset = y - cards_data->y;
      cards_data->point_in_cards = TRUE;
      return TRUE;
    }

  return FALSE;
}

static GdkPixbuf *
gdeck_slot_get_bg_image (GDeckSlot *slot)
{
  GdkPixbuf *pixbuf;

  pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
			   FALSE, 8,
			   GTK_WIDGET (slot)->allocation.width,
			   GTK_WIDGET (slot)->allocation.height);

  if (slot->bg_pixbuf)
    {
      gint i, j;
      gint bg_x, bg_y;
      gint bg_width, bg_height;

      i = 0;
      bg_width = gdk_pixbuf_get_width (slot->bg_pixbuf);
      bg_height = gdk_pixbuf_get_height (slot->bg_pixbuf);
      bg_x = GTK_WIDGET (slot)->allocation.x % bg_width;

      while (i < GTK_WIDGET (slot)->allocation.width)
	{
	  gint width;
	  width = MIN (bg_width - bg_x, GTK_WIDGET (slot)->allocation.width - i);
	  j = 0;
	  bg_y = GTK_WIDGET (slot)->allocation.y % bg_height;
	  while (j < GTK_WIDGET (slot)->allocation.height)
	    {
	      gint height;
	      height = MIN (bg_height - bg_y, GTK_WIDGET (slot)->allocation.height - j);

	      gdk_pixbuf_copy_area (slot->bg_pixbuf,
				    bg_x, bg_y,
				    width, height,
				    pixbuf, i, j);
	      j = j + height;
	      bg_y = 0;
	    }
	  i = i + width;
	  bg_x = 0;
	}
    }
  else
    {
      GdkColor color;
      gint i, j;
      gint width, height;
      guchar *pixels;

      if (slot->color_set)
	color = slot->bg_color;
      else
	color = GTK_WIDGET (slot)->style->bg[GTK_STATE_NORMAL];

      j = 0;
      width = gdk_pixbuf_get_rowstride (pixbuf);
      height = gdk_pixbuf_get_height (pixbuf);
      pixels = gdk_pixbuf_get_pixels (pixbuf);

      while (j < height)
	{
	  i = 0;
	  while (i + 3 < width)
	    {
	      pixels[j*width + i++] = color.red;
	      pixels[j*width + i++] = color.green;
	      pixels[j*width + i++] = color.blue;
	    }
	  j++;
	}
    }

  return pixbuf;
}


/* Public methods */

void
gdeck_slot_set_drag_func (GDeckSlot         *slot,
			  GDeckSlotDragFunc  func,
			  gpointer           data,
			  GtkDestroyNotify   destroy)
{
  g_return_if_fail (GDECK_IS_SLOT (slot));

  if (slot->destroy && slot->data)
    (* slot->destroy) (slot->data);

  slot->func = func;
  slot->data = data;
  slot->destroy = destroy;
}

/**
 * gdeck_slot_set_style:
 * @slot: A #GDeckSlot
 * @style: A #GDeckStyle
 *
 * Sets the drawing style of @slot to be @style.
 *
 **/
void
gdeck_slot_set_style (GDeckSlot  *slot,
		      GDeckStyle *style)
{
  g_return_if_fail (GDECK_IS_SLOT (slot));
  g_return_if_fail (GDECK_IS_STYLE (style));

  g_object_ref (style);
  if (slot->style)
    g_object_unref (slot->style);

  slot->style = style;

  if (GTK_WIDGET_REALIZED (slot))
    gtk_widget_queue_resize (GTK_WIDGET (slot));
}

void
gdeck_slot_set_type (GDeckSlot     *slot,
		     GDeckSlotType  type)
{
  g_return_if_fail (slot != NULL);
  g_return_if_fail (GDECK_IS_SLOT (slot));
  g_return_if_fail (type >= 0 && type < GDECK_SLOT_NUM_TYPES);

  if (slot->type == type)
    return;

  slot->type = type;

  if (GTK_WIDGET_REALIZED (slot))
    gtk_widget_queue_resize (GTK_WIDGET (slot));
}

GList *
gdeck_slot_get_cards (GDeckSlot *slot)
{
  g_return_val_if_fail (slot != NULL, NULL);
  g_return_val_if_fail (GDECK_IS_SLOT (slot), NULL);

  return slot->cards;
}

void
gdeck_slot_add_cards (GDeckSlot *slot,
		      GList     *cards)
{
  g_return_if_fail (slot != NULL);
  g_return_if_fail (GDECK_IS_SLOT (slot));
  g_return_if_fail (cards != NULL);

  if (slot->cards == NULL && cards->next == NULL)
    gtk_widget_queue_draw (GTK_WIDGET (slot));
  else
    gtk_widget_queue_resize (GTK_WIDGET (slot));

  slot->cards = g_list_concat (slot->cards, cards);

  slot->dirty = TRUE;
}

/**
 * gdeck_slot_remove_cards:
 * @slot: A #GDeckSlot
 * @cards: A list of #GDeckCard
 *
 * Removes @cards from the slot.  @cards must be a pointer to the
 * cards that slot has.
 **/
void
gdeck_slot_remove_cards (GDeckSlot  *slot,
			 GList      *cards)
{
  GList *list;

  g_return_if_fail (slot != NULL);
  g_return_if_fail (GDECK_IS_SLOT (slot));

  if (cards == NULL)
    return;

  for (list = cards; list->prev != NULL; list = list->prev);

  g_return_if_fail (list == slot->cards);

  if (slot->cards->next == NULL)
    gtk_widget_queue_draw (GTK_WIDGET (slot));
  else
    gtk_widget_queue_resize (GTK_WIDGET (slot));

  if (cards->prev)
    {
      cards->prev->next = NULL;
      cards->prev = NULL;
    }
  else
    slot->cards = NULL;

  slot->dirty = TRUE;
}

void
gdeck_slot_request_offset (GDeckSlot *slot,
			   gint      *x_offset,
			   gint      *y_offset)
{
  g_return_if_fail (slot != NULL);
  g_return_if_fail (GDECK_IS_SLOT (slot));

  if (x_offset)
    *x_offset = 0;

  if (y_offset)
    *y_offset = 0;
}

void
gdeck_slot_set_back_image (GDeckSlot *slot,
			   GdkPixbuf *background)
{
  g_return_if_fail (slot != NULL);
  g_return_if_fail (GDECK_IS_SLOT (slot));

  if (background != NULL)
    {
      g_return_if_fail (GDK_IS_PIXBUF (background));

      g_object_ref (G_OBJECT (background));
    }
  if (slot->bg_pixbuf)
    g_object_unref (slot->bg_pixbuf);

  slot->bg_pixbuf = background;
  slot->color_set = FALSE;
}

void
gdeck_slot_set_background (GDeckSlot *slot,
			   GdkColor  *color)
{
  g_return_if_fail (slot != NULL);
  g_return_if_fail (GDECK_IS_SLOT (slot));

  if (slot->bg_pixbuf)
    {
      g_object_unref (slot->bg_pixbuf);
      slot->bg_pixbuf = NULL;
    }
  if (color)
    {
      slot->color_set = TRUE;
      slot->bg_color = *color;
    }
  else
    {
      slot->color_set = FALSE;
    }
}
