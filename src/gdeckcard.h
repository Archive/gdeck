/*
 * gdeckcard.h
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_CARD_H__
#define __GDECK_CARD_H__

#include <glib.h>
#include <glib-object.h>
#include "gdeckenums.h"

#define GDECK_TYPE_CARD  (gdeck_card_get_type ())
#define GDECK_CARD(card) ((GDeckCard *)card)

typedef struct _GDeckCard GDeckCard;
struct _GDeckCard
{
  GDeckValue value;
  GDeckSuit suit;
  gboolean face_up;
};


GType      gdeck_card_get_type (void);

GDeckCard *gdeck_card_new      (GDeckValue  value,
				GDeckSuit   suit,
				gboolean    face_up);
void       gdeck_card_free     (GDeckCard  *card);
GDeckCard *gdeck_card_copy     (GDeckCard  *card);
void       gdeck_card_flip     (GDeckCard  *card);
GList     *gdeck_deck_new      (gboolean    face_up);


#endif
