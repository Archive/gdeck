/*
 * gdeckstylefc.h
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_STYLE_FC_H__
#define __GDECK_STYLE_FC_H__

#include <glib-object.h>
#include <libart_lgpl/art_svp.h>
#include <gdk/gdkwindow.h>
#include "gdeckenums.h"
#include "gdeckstyledefault.h"

#define GDECK_TYPE_STYLE_FC            (gdeck_style_fc_get_type ())
#define GDECK_STYLE_FC(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GDECK_TYPE_STYLE_FC, GDeckStyleFC))
#define GDECK_STYLE_FC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GDECK_TYPE_STYLE_FC, GDeckStyleFCClass))
#define GDECK_IS_STYLE_FC(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDECK_TYPE_STYLE_FC))
#define GDECK_IS_STYLE_FC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GDECK_TYPE_STYLE_FC))
#define GDECK_STYLE_FC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GDECK_TYPE_STYLE_FC, GDeckStyleFCClass))


typedef struct _GDeckStyleFC GDeckStyleFC;
typedef struct _GDeckStyleFCClass GDeckStyleFCClass;

struct _GDeckStyleFC
{
  GDeckStyleDefault parent;

  gchar *card_prefix;

  gchar *back_dir;
  gchar *joker_dir;
  gchar *honor_dir;
  gchar *rank_dir;
  gchar *suit_small_dir;
  gchar *suit_medium_dir;
  gchar *suit_large_dir;

  gchar *back_file;
  gchar *joker_file;
  gchar *honor_file;
  gchar *rank_file;
  gchar *suit_small_file;
  gchar *suit_medium_file;
  gchar *suit_large_file;

  GdkPixbuf *back;
  GdkPixbuf *joker;
  GdkPixbuf *honor;
  GdkPixbuf *rank;
  GdkPixbuf *suit_small;
  GdkPixbuf *suit_medium;
  GdkPixbuf *suit_large;
  GdkPixbuf *cards[GDECK_NUM_SUITS][GDECK_NUM_VALUES];
  GdkPixbuf *card_back;

  guint rx, ry;
  guint sx, sy;
  guint x0, x1, x2;
  guint y0, y1, y2, y3, y4, y5, y6, y7, y8;

  guint need_recalc : 1;
};

struct _GDeckStyleFCClass {
  GDeckStyleDefaultClass parent_class;
};


GType       gdeck_style_fc_get_type (void);
GDeckStyle *gdeck_style_fc_new      (void);


#endif
