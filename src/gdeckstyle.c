/*
 * gdeckstyle.c
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gdeckstyle.h"

#include "libart_lgpl/art_vpath.h"
#include "libart_lgpl/art_bpath.h"
#include "libart_lgpl/art_vpath_bpath.h"
#include "libart_lgpl/art_svp.h"
#include "libart_lgpl/art_svp_vpath.h"
#include "libart_lgpl/art_rgb_svp.h"
#include <string.h>

static void gdeck_style_init         (GDeckStyle      *style);
static void gdeck_style_class_init   (GDeckStyleClass *class);
static void gdeck_style_set_property (GObject         *object,
				      guint            prop_id,
				      const GValue    *value,
				      GParamSpec      *pspec);
static void gdeck_style_get_property (GObject         *object,
				      guint            prop_id,
				      GValue          *value,
				      GParamSpec      *pspec);


static GObjectClass *parent_class = NULL;


/* Properties */
enum {
  PROP_0,
  PROP_WIDTH,
  PROP_HEIGHT,
  PROP_VSLOT_OFFSET,
  PROP_HSLOT_OFFSET,
  PROP_VSLOT_HIDDEN_OFFSET,
  PROP_HSLOT_HIDDEN_OFFSET,
  PROP_ZOOM_LEVEL,
};


GType
gdeck_style_get_type (void)
{
     static GType style_type = 0;

     if (! style_type)
     {
	  static const GTypeInfo style_info =
	  {
	       sizeof (GDeckStyleClass),
	       NULL,           /* base_init */
	       NULL,           /* base_finalize */
	       (GClassInitFunc) gdeck_style_class_init,
	       NULL,           /* class_finalize */
	       NULL,           /* class_data */
	       sizeof (GDeckStyle),
	       0,             /* n_preallocs */
	       (GInstanceInitFunc) gdeck_style_init,
	  };

	  style_type = g_type_register_static (G_TYPE_OBJECT, "GDeckStyle", &style_info, 0);
     }

     return style_type;
}

/* object members */
static void
gdeck_style_init (GDeckStyle *style)
{
  style->width = 89;
  style->height = 133;
  style->vslot_offset = 20;
  style->hslot_offset = 20;
  style->vslot_hidden_offset = 10;
  style->hslot_hidden_offset = 10;

  style->drag_button = 1;
  style->activate_button = 2;
  style->reveal_button = 3;
}


static void
gdeck_style_class_init (GDeckStyleClass *class)
{
  GObjectClass *o_class = (GObjectClass *) class;
  parent_class = g_type_class_peek_parent (class);

  o_class->get_property = gdeck_style_get_property;
  o_class->set_property = gdeck_style_set_property;

  g_object_class_install_property (o_class,
                                   PROP_WIDTH,
                                   g_param_spec_int ("width",
						     "GDeckStyle Width",
						     "The width of the card",
						     0,
						     500,
						     89,
						     G_PARAM_READWRITE));

  g_object_class_install_property (o_class,
                                   PROP_HEIGHT,
                                   g_param_spec_int ("height",
						     "GDeckStyle Height",
						     "The height of the card",
						     0,
						     500,
						     133,
						     G_PARAM_READWRITE));

  g_object_class_install_property (o_class,
                                   PROP_VSLOT_OFFSET,
                                   g_param_spec_int ("vslot_offset",
						     "GDeckStyle VSlot Offset",
						     "The offset of vertically placed cards",
						     0,
						     500,
						     20,
						     G_PARAM_READWRITE));

  g_object_class_install_property (o_class,
                                   PROP_HSLOT_OFFSET,
                                   g_param_spec_int ("hslot_offset",
						     "GDeckStyle HSlot Ofest",
						     "The offset of horizontally placed cards",
						     0,
						     500,
						     20,
						     G_PARAM_READWRITE));

  g_object_class_install_property (o_class,
				   PROP_VSLOT_HIDDEN_OFFSET,
                                   g_param_spec_int ("vslot_hidden_offset",
						     "GDeckStyle VSlot Hidden Ofest",
						     "The offset of vertically placed face down cards",
						     0,
						     500,
						     10,
						     G_PARAM_READWRITE));

  g_object_class_install_property (o_class,
				   PROP_HSLOT_HIDDEN_OFFSET,
                                   g_param_spec_int ("hslot_hidden_offset",
						     "GDeckStyle HSlot Hidden Ofest",
						     "The offset of horizontally placed face down cards",
						     0,
						     500,
						     10,
						     G_PARAM_READWRITE));
}


static void
gdeck_style_set_property (GObject      *object,
			  guint         prop_id,
			  const GValue *value,
			  GParamSpec   *pspec)
{

}

static void
gdeck_style_get_property (GObject    *object,
			  guint       prop_id,
			  GValue     *value,
			  GParamSpec *pspec)
{

}

GDeckStyle *
gdeck_style_new (void)
{
  return GDECK_STYLE (g_object_new (GDECK_TYPE_STYLE, NULL));
}


GdkPixbuf *
gdeck_style_get_mask (GDeckStyle *style)
{
  g_return_val_if_fail (GDECK_IS_STYLE (style), NULL);
  g_return_val_if_fail (GDECK_STYLE_GET_CLASS (style)->get_mask != NULL, NULL);

  return (* GDECK_STYLE_GET_CLASS (style)->get_mask) (style);
}

GdkPixbuf *
gdeck_style_get_blank (GDeckStyle *style)
{
  g_return_val_if_fail (GDECK_IS_STYLE (style), NULL);
  g_return_val_if_fail (GDECK_STYLE_GET_CLASS (style)->get_blank != NULL, NULL);

  return (* GDECK_STYLE_GET_CLASS (style)->get_blank) (style);
}

void
gdeck_style_render_slot (GDeckStyle      *style,
			 GdkPixbuf       *pixbuf,
			 GDeckSlotType    type,
			 GDeckSlotLayout  layout,
			 GDeckState       state)
{
  g_return_if_fail (GDECK_IS_STYLE (style));
  g_return_if_fail (GDECK_STYLE_GET_CLASS (style)->render_slot != NULL);

  (* GDECK_STYLE_GET_CLASS (style)->render_slot) (style, pixbuf, type, layout, state);
}

void
gdeck_style_render_card (GDeckStyle    *style,
			 GdkPixbuf     *pixbuf,
			 GDeckCard     *card,
			 gint           x,
			 gint           y,
			 GDeckSlotType  type,
			 GDeckState     state,
			 gboolean       next_card)
{
  g_return_if_fail (GDECK_IS_STYLE (style));
  g_return_if_fail (GDECK_STYLE_GET_CLASS (style)->render_card != NULL);

  (* GDECK_STYLE_GET_CLASS (style)->render_card) (style, pixbuf,
						  card, x, y,
						  type, state,
						  next_card);
}

void
gdeck_style_generate_cards (GDeckStyle *style,
			    GError     *error)
{
  g_return_if_fail (GDECK_IS_STYLE (style));
  g_return_if_fail (GDECK_STYLE_GET_CLASS (style)->generate_cards != NULL);

  (* GDECK_STYLE_GET_CLASS (style)->generate_cards) (style, error);
}

void
gdeck_style_map_cards (GDeckStyle        *style,
		       GDeckSlotLayout    layout,
		       GList             *cards,
		       GDeckStyleMapFunc  func,
		       gpointer           data)
{
  GList *list = cards;
  gint width_offset = 0;
  gint height_offset = 0;
  gint segment_width;
  gint segment_height;
  gint x = 0;
  gint y = 0;
  gint width = 0;
  gint height = 0;

  g_return_if_fail (style != NULL);
  g_return_if_fail (GDECK_IS_STYLE (style));
  g_return_if_fail (func != NULL);

  if (list == NULL)
    return;

  switch (layout)
    {
    case GDECK_SLOT_CENTER:
      list = g_list_last (cards);
      break;
    case GDECK_SLOT_RIGHT:
    case GDECK_SLOT_LEFT:
      width_offset = 1;
      break;
    case GDECK_SLOT_DOWN:
    case GDECK_SLOT_UP:
      height_offset = 1;
      break;
    default:
      g_assert_not_reached ();
      return;
    }

  while (list)
    {
      if (list->next == NULL)
	{
	  segment_width = style->width;
	  segment_height = style->height;
	}
      else
	{
	  if (GDECK_CARD (list->data)->face_up)
	    {
	      width = width_offset * style->hslot_offset;
	      height = height_offset * style->vslot_offset;
	    }
	  else
	    {
	      width = width_offset * style->hslot_hidden_offset;
	      height = height_offset * style->vslot_hidden_offset;
	    }

	  if (width == 0)
	    {
	      segment_width = style->width;
	      segment_height = height;
	    }
	  else
	    {
	      segment_width = width;
	      segment_height = style->height;
	    }
	}

      if ((* func) (style, layout, list, x, y, segment_width, segment_height, data))
	return;

      x += width;
      y += height;
      list = list->next;
    }
}

static gint
render_cards_helper (GDeckStyle      *style,
		     GDeckSlotLayout  layout,
		     GList           *cards,
		     gint             x,
		     gint             y,
		     gint             width,
		     gint             height,
		     gpointer         data)
{
  gdeck_style_render_card (style,
			   GDK_PIXBUF (data),
			   GDECK_CARD (cards->data),
			   x, y, layout,
			   GDECK_STATE_NORMAL,
			   cards->next != NULL);

  return FALSE;
}

void
gdeck_style_render_cards (GDeckStyle       *style,
			  GDeckSlotLayout   layout,
			  GdkPixbuf       **pixbuf,
			  GList            *cards)
{
  g_return_if_fail (style != NULL);
  g_return_if_fail (GDECK_IS_STYLE (style));
  g_return_if_fail (cards != NULL);
  g_return_if_fail (pixbuf != NULL);

  if (*pixbuf == NULL)
    {
      gint width, height;
      gdeck_style_get_size (style, layout, cards, &width, &height);
      *pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
				TRUE, 8, width, height);
      memset (gdk_pixbuf_get_pixels (*pixbuf), 0,
	      gdk_pixbuf_get_rowstride (*pixbuf) * height);
    }
  gdeck_style_map_cards (style, layout, cards, render_cards_helper, *pixbuf);
}

static gboolean
get_size_helper (GDeckStyle      *style,
		 GDeckSlotLayout  layout,
		 GList           *cards,
		 gint             x,
		 gint             y,
		 gint             width,
		 gint             height,
		 gpointer         data)
{
  if (cards->next == NULL)
    {
      ((GdkRectangle *) data)->width = style->width + x;
      ((GdkRectangle *) data)->height = style->height + y;

      return TRUE;
    }
  return FALSE;
}

void
gdeck_style_get_size (GDeckStyle      *style,
		      GDeckSlotLayout  layout,
		      GList           *cards,
		      gint            *width,
		      gint            *height)
{
  GdkRectangle rect;

  g_return_if_fail (style != NULL);
  g_return_if_fail (GDECK_IS_STYLE (style));
  g_return_if_fail (cards != NULL);

  rect.width = style->width;
  rect.height = style->height;

  gdeck_style_map_cards (style, layout, cards, get_size_helper, &rect);

  *width = rect.width;
  *height = rect.height;
}
