#include "gdk-pixbuf-rotate.h"
#include <string.h>
#include <math.h>

#ifndef G_PI
#define G_PI    3.14159265358979323846E0
#endif

GdkPixbuf *
gdk_pixbuf_rotate_0 (GdkPixbuf *pixbuf)
{
  return gdk_pixbuf_copy (pixbuf);
}

GdkPixbuf *
gdk_pixbuf_rotate_90 (GdkPixbuf *pixbuf)
{
  GdkPixbuf *result;
  int height = gdk_pixbuf_get_height(pixbuf);
  int width = gdk_pixbuf_get_width(pixbuf);
  int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
  int result_rowstride;
  guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
  guchar *result_pixels;
  guchar *row_buffer;
  const int bytes_per_pixel = (gdk_pixbuf_get_n_channels(pixbuf)*gdk_pixbuf_get_bits_per_sample(pixbuf))/8;
  register int i, j;

  result = gdk_pixbuf_new (gdk_pixbuf_get_colorspace(pixbuf),
			   gdk_pixbuf_get_has_alpha(pixbuf),
			   gdk_pixbuf_get_bits_per_sample(pixbuf),
			   height,
			   width);
  result_rowstride = gdk_pixbuf_get_rowstride (result);

  result_pixels = gdk_pixbuf_get_pixels (result);

  row_buffer = g_malloc (result_rowstride);

  for (i = 0; i < width; i++)
    {
      for (j = 0; j < height; j++)
	{
	  memcpy (row_buffer + (bytes_per_pixel * ((height - 1) - j)),
		  pixels + (j * rowstride) + (i * bytes_per_pixel),
		  bytes_per_pixel);
	}
      memcpy (result_pixels + (i * result_rowstride), row_buffer, result_rowstride);
    }

  g_free (row_buffer);

  return result;
}


GdkPixbuf *
gdk_pixbuf_rotate_180 (GdkPixbuf *pixbuf)
{
  GdkPixbuf *result;
  const int height = gdk_pixbuf_get_height(pixbuf);
  const int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
  const int width = gdk_pixbuf_get_width(pixbuf);
  const int channels = gdk_pixbuf_get_n_channels(pixbuf);
  const int bits_per_sample = gdk_pixbuf_get_bits_per_sample(pixbuf);
  const int bytes_per_pixel = (gdk_pixbuf_get_n_channels(pixbuf)*gdk_pixbuf_get_bits_per_sample(pixbuf))/8;
  guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
  guchar *result_pixels;
  int i, j;
  int row_size;

  result = gdk_pixbuf_new(gdk_pixbuf_get_colorspace(pixbuf),
			  gdk_pixbuf_get_has_alpha(pixbuf),
			  gdk_pixbuf_get_bits_per_sample(pixbuf),
			  width,
			  height);

  result_pixels = gdk_pixbuf_get_pixels(result);

  row_size = width * ((channels * bits_per_sample + 7) / 8);

  for(i = 0; i < height; i++) {
    for (j = 0; j < width; j++) {
      memcpy(result_pixels+(((height-1)-i)*rowstride) + ((width-1)-j)*bytes_per_pixel, pixels+(i*rowstride) + j*bytes_per_pixel, bytes_per_pixel);
    }
  }
  return result;
}


GdkPixbuf *
gdk_pixbuf_rotate_270 (GdkPixbuf *pixbuf)
{
  GdkPixbuf *result;
  int height = gdk_pixbuf_get_height(pixbuf);
  int width = gdk_pixbuf_get_width(pixbuf);
  int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
  int result_rowstride;
  guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
  guchar *result_pixels;
  guchar *row_buffer;
  const int bytes_per_pixel = (gdk_pixbuf_get_n_channels(pixbuf)*gdk_pixbuf_get_bits_per_sample(pixbuf))/8;
  register int i, j;

  result = gdk_pixbuf_new(gdk_pixbuf_get_colorspace(pixbuf),
			  gdk_pixbuf_get_has_alpha(pixbuf),
			  gdk_pixbuf_get_bits_per_sample(pixbuf),
			  height,
			  width);
  result_rowstride = gdk_pixbuf_get_rowstride(result);


  result_pixels = gdk_pixbuf_get_pixels(result);

  row_buffer = g_malloc(result_rowstride);

  for(i = 0; i < width; i++) {

    for(j = 0; j < height; j++) {
      memcpy(row_buffer+(bytes_per_pixel*(j)), pixels+(j*rowstride)+(i*bytes_per_pixel), bytes_per_pixel);
    }
    memcpy(result_pixels+(((width-1)-i)*result_rowstride), row_buffer, result_rowstride);
  }
  g_free(row_buffer);

  return result;
}
#if 0
GdkPixbuf *
gdk_pixbuf_rotate (GdkPixbuf *pixbuf,
		   gdouble    angle)
{
  gint src_height, src_width; 
  int src_rowstride;
  guchar *src_pixels;
  const int src_bytes_per_pixel;

  GdkPixbuf *retval;
  guchar *retval_pixels;
  int retval_rowstride;
  gint retval_height;
  gint retval_width;
  guchar *row_buffer;

  gdouble height, width;
  gdouble cos_a, sin_a;

  g_return_val_if_fail (GDK_IS_PIXBUF (pixbuf), NULL);
  g_return_val_if_fail (angle <= G_PI/2 && angle >= -G_PI/2, NULL);

  src_height = gdk_pixbuf_get_height (pixbuf);
  src_width = gdk_pixbuf_get_width (pixbuf);  
  src_rowstride = gdk_pixbuf_get_rowstride (pixbuf);
  src_pixels = gdk_pixbuf_get_pixels (pixbuf);
  src_bytes_per_pixel = (gdk_pixbuf_get_n_channels (pixbuf) * gdk_pixbuf_get_bits_per_sample (pixbuf)) / 8;

  cos_a = cos (angle);
  sin_a = sin (angle);

  retval_width = height * ABS (sin (angle)) + width * ABS (cos (angle));
  retval_height = width * ABS (sin (angle)) + height * ABS (cos (angle));

  retval = gdk_pixbuf_new (gdk_pixbuf_get_colorspace (pixbuf),
			   gdk_pixbuf_get_has_alpha (pixbuf),
			   gdk_pixbuf_get_bits_per_sample (pixbuf),
			   CIEL (retval_width),
			   CIEL (retval_height));
  retval_pixels = gdk_pixbuf_get_pixels (retval);
  memset (retval_pixels, 0, gdk_pixbuf_get_rowstride (retval) * gdk_pixbuf_get_height (retval));


  
  for (i = 0; i < width; i++)
    {
      for (j = 0; j < height; j++)
	{
	  
	}
    }
  return retval;
}
#endif
