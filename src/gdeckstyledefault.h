/*
 * gdeckstyledefault.h
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_STYLE_DEFAULT_H__
#define __GDECK_STYLE_DEFAULT_H__

#include <glib-object.h>
#include <gdk/gdkwindow.h>
#include <libart_lgpl/art_svp.h>
#include "gdeckstyle.h"

#define GDECK_TYPE_STYLE_DEFAULT            (gdeck_style_default_get_type ())
#define GDECK_STYLE_DEFAULT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GDECK_TYPE_STYLE_DEFAULT, GDeckStyleDefault))
#define GDECK_STYLE_DEFAULT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GDECK_TYPE_STYLE_DEFAULT, GDeckStyleDefaultClass))
#define GDECK_IS_STYLE_DEFAULT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDECK_TYPE_STYLE_DEFAULT))
#define GDECK_IS_STYLE_DEFAULT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GDECK_TYPE_STYLE_DEFAULT))
#define GDECK_STYLE_DEFAULT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GDECK_TYPE_STYLE_DEFAULT, GDeckStyleDefaultClass))


typedef struct _GDeckStyleDefault GDeckStyleDefault;
typedef struct _GDeckStyleDefaultClass GDeckStyleDefaultClass;

struct _GDeckStyleDefault
{
  GDeckStyle parent;

  gint radius;

  guint outline_color;  /*black */

  guint slot_fill_color;
  guint slot_highlight_color;
  guint slot_stock_used_color;
  guint slot_selected_color;

  guint card_fill_color;

  
  unsigned int fill_set : 1;	/* Is fill color set? */
  unsigned int outline_set : 1;	/* Is outline color set? */

  ArtSVP *fill_svp;		/* The SVP for the filled shape */
  ArtSVP *outline_svp;		/* The SVP for the outline shape */
  ArtSVP *stock_svp;
  ArtSVP *waste_svp;

  GdkPixbuf *mask;
  GdkPixbuf *blank;
  GdkPixbuf *bg_pixbuf;
};

struct _GDeckStyleDefaultClass {
     GDeckStyleClass parent_class;
};


GType       gdeck_style_default_get_type (void);
GDeckStyle *gdeck_style_default_new      (void);


#endif
