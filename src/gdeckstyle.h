/*
 * gdeckstyle.h
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GDECK_STYLE_H__
#define __GDECK_STYLE_H__

#include "gdeckenums.h"
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glib-object.h>
#include <gdk/gdkwindow.h>
#include <libart_lgpl/art_svp.h>
#include "gdeckcard.h"

#define GDECK_TYPE_STYLE            (gdeck_style_get_type ())
#define GDECK_STYLE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GDECK_TYPE_STYLE, GDeckStyle))
#define GDECK_STYLE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GDECK_TYPE_STYLE, GDeckStyleClass))
#define GDECK_IS_STYLE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDECK_TYPE_STYLE))
#define GDECK_IS_STYLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GDECK_TYPE_STYLE))
#define GDECK_STYLE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GDECK_TYPE_STYLE, GDeckStyleClass))



typedef struct _GDeckStyle GDeckStyle;
typedef struct _GDeckStyleClass GDeckStyleClass;

typedef gint (* GDeckStyleMapFunc) (GDeckStyle      *style,
				    GDeckSlotLayout  layout,
				    GList           *card_list, 
				    gint             x,
				    gint             y,
				    gint             width,
				    gint             height,
				    gpointer         data);

struct _GDeckStyle
{
  GObject parent;

  gint width;
  gint height;
  gint vslot_offset;
  gint hslot_offset;
  gint vslot_hidden_offset;
  gint hslot_hidden_offset;
  gdouble zoom_level;

  /* button settings */
  gint drag_button;
  gint activate_button;
  gint reveal_button;
};

struct _GDeckStyleClass {
  GObjectClass parent_class;

  /* signals */
  void       (* size_changed) (GDeckStyle      *style);
  void       (* back_changed) (GDeckStyle      *style);
  void       (* card_changed) (GDeckStyle      *style,
			       GDeckCard       *card);
  /* vtable */
  GdkPixbuf *(* get_mask)    (GDeckStyle      *style);
  GdkPixbuf *(* get_blank)   (GDeckStyle      *style);
  void       (* render_slot) (GDeckStyle      *style,
			      GdkPixbuf       *pixbuf,
			      GDeckSlotType    type,
			      GDeckSlotLayout  layout,
			      GDeckState       state);
  void       (* render_card) (GDeckStyle      *style,
			      GdkPixbuf       *pixbuf,
			      GDeckCard       *card,
			      gint             x,
			      gint             y,
			      GDeckSlotType    type,
			      GDeckState       state,
			      gboolean         next_card);
  void       (* generate_cards ) (GDeckStyle *style,
				  GError *error);
};

GType      gdeck_style_get_type       (void);

GdkPixbuf *gdeck_style_get_mask       (GDeckStyle         *style);
GdkPixbuf *gdeck_style_get_blank      (GDeckStyle         *style);
void       gdeck_style_render_slot    (GDeckStyle         *style,
				       GdkPixbuf          *pixbuf,
				       GDeckSlotType       type,
				       GDeckSlotLayout     layout,
				       GDeckState          state);
void       gdeck_style_render_card    (GDeckStyle         *style,
				       GdkPixbuf          *pixbuf,
				       GDeckCard          *card,
				       gint                x,
				       gint                y,
				       GDeckSlotLayout     layout,
				       GDeckState          state,
				       gboolean            next_card);
void       gdeck_style_generate_cards (GDeckStyle         *style,
				       GError             *error);

/* Helper functions */
void       gdeck_style_map_cards      (GDeckStyle         *style,
				       GDeckSlotLayout     layout,
				       GList              *cards,
				       GDeckStyleMapFunc   func,
				       gpointer            data);
void       gdeck_style_render_cards   (GDeckStyle         *style,
				       GDeckSlotLayout     layout,
				       GdkPixbuf         **pixbuf,
				       GList              *cards);
void       gdeck_style_get_size       (GDeckStyle         *style,
				       GDeckSlotLayout     layout,
				       GList              *cards,
				       gint               *width,
				       gint               *height);


#endif
