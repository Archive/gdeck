/*
 * gdeckstylefc.c
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gdeckstylefc.h"
#include "gdeckstyledefaultpaths.h"
#include "gdk-pixbuf-rotate.h"
#include <libart_lgpl/art_rgb_affine.h>

#include <string.h>
#include <math.h>

/* gdk-card-image.c calculates these in calculate_dimensions.  As we never got
 * another set of images, it's safe to hard code it and deprecate this module.
 */

#define XDELTA 15
#define WIDTH 79
#define HEIGHT 123
#define CORNER 12
#define YDELTA 12

#define DRAW_ELEMENT(src_pixbuf) gdeck_draw_element (src_pixbuf, pixbuf, src_x, src_y, src_width, src_height, dest_x, dest_y)
#define COMPOSE_ELEMENT(src_pixbuf) gdeck_compose_element (src_pixbuf, pixbuf, src_x, src_y, src_width, src_height, dest_x, dest_y)

static void       gdeck_style_fc_init                (GDeckStyleFC      *style);
static void       gdeck_style_fc_class_init          (GDeckStyleFCClass *class);

static void       gdeck_real_style_fc_render_card    (GDeckStyle        *style,
						      GdkPixbuf         *pixbuf,
						      GDeckCard         *card,
						      gint               x,
						      gint               y,
						      GDeckSlotLayout    type,
						      GDeckState         state,
						      gboolean           next_card);
static void       gdeck_real_style_fc_generate_cards (GDeckStyle        *style,
						      GError            *error);



static GDeckStyleDefault *parent_class = NULL;


GType
gdeck_style_fc_get_type (void)
{
  static GType style_type = 0;

  if (! style_type)
    {
      static const GTypeInfo style_info =
      {
	sizeof (GDeckStyleFCClass),
	NULL,           /* base_init */
	NULL,           /* base_finalize */
	(GClassInitFunc) gdeck_style_fc_class_init,
	NULL,           /* class_finalize */
	NULL,           /* class_data */
	sizeof (GDeckStyleFC),
	0,             /* n_preallocs */
	(GInstanceInitFunc) gdeck_style_fc_init,
      };

      style_type = g_type_register_static (GDECK_TYPE_STYLE_DEFAULT, "GDeckStyleFC", &style_info, 0);
    }

  return style_type;
}

/* object members */
static void
gdeck_style_fc_init (GDeckStyleFC *style)
{
  /* initialize dirs */
  /* change to $DATADIR/pixmaps */
  style->card_prefix = g_strdup ("/usr/share/pixmaps");

  style->back_dir = g_strdup ("cards/backs");
  style->joker_dir = g_strdup ("cards/jokers");
  style->honor_dir = g_strdup ("cards/honors");
  style->rank_dir = g_strdup ("cards/ranks");
  style->suit_small_dir = g_strdup ("cards/suits_small");
  style->suit_medium_dir = strdup ("cards/suits_medium");
  style->suit_large_dir = g_strdup ("cards/suits_large");

  style->back_file = g_strdup ("beige.png");
  style->joker_file = g_strdup ("gnome.png");
  style->honor_file = g_strdup ("bonded.png");
  style->rank_file = g_strdup ("bold-09x14.png");
  style->suit_small_file = g_strdup ("knuth-09x10.png");
  style->suit_medium_file = strdup ("knuth-18x21.png");
  style->suit_large_file = g_strdup ("knuth-21x25.png");

  GDECK_STYLE (style)->width = WIDTH;
  GDECK_STYLE (style)->height = HEIGHT;
  GDECK_STYLE_DEFAULT (style)->radius = CORNER;

  style->need_recalc = TRUE;
}

static void
gdeck_style_fc_class_init (GDeckStyleFCClass *class)
{
  GDeckStyleClass *style_class = (GDeckStyleClass *) class;

  parent_class = g_type_class_peek_parent (class);

  style_class->render_card = gdeck_real_style_fc_render_card;
  style_class->generate_cards = gdeck_real_style_fc_generate_cards;
}

GDeckStyle *
gdeck_style_fc_new (void)
{
  return GDECK_STYLE (g_object_new (GDECK_TYPE_STYLE_FC, NULL));
}

static void
gdeck_compose_element (GdkPixbuf *src_image,
		       GdkPixbuf *dest_image,
		       gint       src_x,
		       gint       src_y,
		       gint       src_width,
		       gint       src_height,
		       gint       dest_x,
		       gint       dest_y)
{
  gdk_pixbuf_composite (src_image,
			dest_image,
			dest_x, dest_y,
			src_width, src_height,
			(gfloat)dest_x - src_x,
			(gfloat)dest_y - src_y,
			1.0, 1.0,
			GDK_INTERP_BILINEAR,
			255);
}

static void
gdeck_draw_element (GdkPixbuf *src_image,
		    GdkPixbuf *dest_image,
		    gint       src_x,
		    gint       src_y,
		    gint       src_width,
		    gint       src_height,
		    gint       dest_x,
		    gint       dest_y)
{
  gdk_pixbuf_copy_area (src_image,
			src_x, src_y,
			src_width, src_height,
			dest_image,
			dest_x, dest_y);
}

static void
gdeck_real_style_fc_render_card (GDeckStyle      *style,
				 GdkPixbuf       *pixbuf,
				 GDeckCard       *card,
				 gint             x,
				 gint             y,
				 GDeckSlotLayout  layout,
				 GDeckState       state,
				 gboolean         next_card)
{
  GDeckStyleFC *fc_style = GDECK_STYLE_FC (style);
  gint width, height;
  gint radius = GDECK_STYLE_DEFAULT (style)->radius;
  GdkPixbuf *src;

  if (next_card)
    {
      switch (layout)
	{
	case GDECK_SLOT_CENTER:
	  width = style->width;
	  height = style->height;
	  break;
	case GDECK_SLOT_RIGHT:
	case GDECK_SLOT_LEFT:
	  height = style->height;
	  if (card->face_up)
	    width = style->hslot_offset + radius;
	  else
	    width = style->hslot_hidden_offset + radius;
	  break;
	case GDECK_SLOT_DOWN:
	case GDECK_SLOT_UP:
	  width = style->width;
	  if (card->face_up)
	    height = style->vslot_offset + radius;
	  else
	    height = style->vslot_hidden_offset + radius;
	  break;
	default:
	  g_assert_not_reached ();
	  return;
	}
    }
  else
    {
      width = style->width;
      height = style->height;
    }

  if (card->face_up)
    src = fc_style->cards[card->suit][card->value];
  else
    src = fc_style->card_back;

  gdk_pixbuf_composite (src,
			pixbuf,
			x, y,
			width,
			height,
			x, y, 1.0, 1.0,
			GDK_INTERP_BILINEAR,
			255);
}
static void
gdeck_real_style_fc_draw_rank (GDeckStyleFC *fc_style,
			       GdkPixbuf    *rank,
			       GdkPixbuf    *rank_rotated,
			       GDeckSuit     suit,
			       GDeckValue    value)
{
  GdkPixbuf *pixbuf = fc_style->cards[suit][value];
  gint dest_x, dest_y;
  gint src_width, src_height;
  gdouble src_x, src_y;


  src_width = gdk_pixbuf_get_width (rank)/14;
  src_height = gdk_pixbuf_get_height (rank)/2;

  if (value == GDECK_JOKER_1 || value == GDECK_JOKER_2)
    src_x = 0;
  else if (value == GDECK_ACE_HIGH)
    src_x = src_width;
  else
    src_x = ((gdouble)src_width * value);

  if (suit == GDECK_SPADE || suit == GDECK_CLUB)
    src_y = 0;
  else
    src_y = src_height;
  dest_x = fc_style->rx;
  dest_y = fc_style->ry;

  DRAW_ELEMENT (rank);

  dest_x = WIDTH - fc_style->rx - src_width;
  dest_y = fc_style->ry;
  DRAW_ELEMENT (rank);

  if (value == GDECK_JOKER_1 || value == GDECK_JOKER_2)
    src_x = src_width * 13;
  else if (value == GDECK_ACE_HIGH)
    src_x = src_width * 12;
  else
    src_x = ((gdouble)src_width * (13 - value));

  if (suit == GDECK_SPADE || suit == GDECK_CLUB)
    src_y = src_height;
  else
    src_y = 0;

  dest_x = fc_style->rx;
  dest_y = HEIGHT - fc_style->ry - src_height;
  DRAW_ELEMENT (rank_rotated);

  dest_x = WIDTH - fc_style->rx - src_width;
  dest_y = HEIGHT - fc_style->ry - src_height;
  DRAW_ELEMENT (rank_rotated);
}

static void
gdeck_real_style_fc_draw_suit_small (GDeckStyleFC *fc_style,
				     GdkPixbuf    *suit_small,
				     GdkPixbuf    *suit_small_rotated,
				     GDeckSuit     suit,
				     GDeckValue    value)
{
  GdkPixbuf *pixbuf = fc_style->cards[suit][value];
  gint dest_x, dest_y;
  gint src_width, src_height;
  gdouble src_x, src_y;


  src_width = gdk_pixbuf_get_width (suit_small)/4;
  src_height = gdk_pixbuf_get_height (suit_small);

  src_x = suit * src_width;
  src_y = 0;

  dest_x = fc_style->sx;
  dest_y = fc_style->sy;
  DRAW_ELEMENT (suit_small);

  dest_x = WIDTH - fc_style->sx - src_width;
  dest_y = fc_style->sy;
  DRAW_ELEMENT (suit_small);

  src_x = ((gdouble)src_width * (3 - suit));

  dest_x = fc_style->sx;
  dest_y = HEIGHT - fc_style->sy - src_height;
  DRAW_ELEMENT (suit_small_rotated);

  dest_x = WIDTH - fc_style->sx - src_width;
  dest_y = HEIGHT - fc_style->sy - src_height;
  DRAW_ELEMENT (suit_small_rotated);
}

static void
gdeck_real_style_fc_draw_ace (GDeckStyleFC *fc_style,
			      GdkPixbuf    *suit_large,
			      GDeckSuit     suit,
			      GDeckValue    value)
{
  GdkPixbuf *pixbuf = fc_style->cards[suit][value];
  gint dest_x, dest_y;
  gint src_width, src_height;
  gdouble src_x, src_y;


  src_width = gdk_pixbuf_get_width (suit_large)/4;
  src_height = gdk_pixbuf_get_height (suit_large);
  src_x = src_width * suit;
  src_y = 0;
  dest_x = (WIDTH - src_width)/2;
  dest_y = (HEIGHT - src_height)/2;

  DRAW_ELEMENT (suit_large);
}

static void
gdeck_real_style_fc_draw_honors (GDeckStyleFC *fc_style,
				 GdkPixbuf    *honor,
				 GdkPixbuf    *honor_rotated,
				 GDeckSuit     suit,
				 GDeckValue    value)
{
  GdkPixbuf *pixbuf = fc_style->cards[suit][value];
  gint dest_x, dest_y;
  gint src_width, src_height;
  gdouble src_x, src_y;

  src_width = gdk_pixbuf_get_width (honor)/4;
  src_height = gdk_pixbuf_get_height (honor)/3;
  dest_x = (WIDTH - src_width)/2;
  dest_y = (HEIGHT - 2*src_height)/2;
  src_x = suit * src_width;
  src_y = (value - GDECK_JACK) * src_height;
  DRAW_ELEMENT (honor);

  dest_y += src_height;
  src_x = (GDECK_NUM_SUITS - (suit+1)) * src_width;
  src_y = ((gdouble)GDECK_KING - value) * src_height;
  DRAW_ELEMENT (honor_rotated);
}

static void
gdeck_real_style_fc_draw_number_cards (GDeckStyleFC *fc_style,
				       GdkPixbuf    *suit_medium_rotated,
				       GDeckSuit     suit,
				       GDeckValue    value)
{
  GdkPixbuf *pixbuf = fc_style->cards[suit][value];
  gint dest_x, dest_y;
  gint src_width, src_height;
  gint normal_src_x;
  gint rotated_src_x;
  gdouble src_x, src_y;

  src_width = gdk_pixbuf_get_width (fc_style->suit_medium)/4;
  src_height = gdk_pixbuf_get_height (fc_style->suit_medium);
  normal_src_x = src_x = src_width * suit;
  src_y = 0;
  rotated_src_x = src_width * (3 - suit);

  if (value == GDECK_TWO ||
      value == GDECK_THREE)
    {
      dest_x = fc_style->x1;
      dest_y = fc_style->y0;
      COMPOSE_ELEMENT (fc_style->suit_medium);
      if (value == GDECK_THREE)
	{
	  dest_y = fc_style->y3;
	  COMPOSE_ELEMENT (fc_style->suit_medium);
	}
      src_x = rotated_src_x;
      dest_y = fc_style->y6;
      COMPOSE_ELEMENT (suit_medium_rotated);
    }
  else
    {
      dest_x = fc_style->x0;
      dest_y = fc_style->y0;
      COMPOSE_ELEMENT (fc_style->suit_medium);
      dest_x = fc_style->x2;
      COMPOSE_ELEMENT (fc_style->suit_medium);
      src_x = rotated_src_x;
      dest_x = fc_style->x0;
      dest_y = fc_style->y6;
      COMPOSE_ELEMENT (suit_medium_rotated);
      dest_x = fc_style->x2;
      COMPOSE_ELEMENT (suit_medium_rotated);
      src_x = normal_src_x;
      switch (value)
	{
	case GDECK_FOUR:
	  break;
	case GDECK_FIVE:
	  dest_x = fc_style->x1;
	  dest_y = fc_style->y3;
	  COMPOSE_ELEMENT (fc_style->suit_medium);
	  break;
	case GDECK_SIX:
	case GDECK_SEVEN:
	case GDECK_EIGHT:
	  dest_x = fc_style->x0;
	  dest_y = fc_style->y3;
	  COMPOSE_ELEMENT (fc_style->suit_medium);
	  dest_x = fc_style->x2;
	  COMPOSE_ELEMENT (fc_style->suit_medium);
	  if (value == GDECK_SIX)
	    break;
	  dest_x = fc_style->x1;
	  dest_y = fc_style->y7;
	  COMPOSE_ELEMENT (fc_style->suit_medium);
	  if (value == GDECK_SEVEN)
	    break;
	  src_x = rotated_src_x;
	  dest_y = fc_style->y8;
	  COMPOSE_ELEMENT (suit_medium_rotated);
	  break;
	case GDECK_NINE:
	case GDECK_TEN:
	  dest_x = fc_style->x0;
	  dest_y = fc_style->y2;
	  COMPOSE_ELEMENT (fc_style->suit_medium);
 	  dest_x = fc_style->x2;
	  COMPOSE_ELEMENT (fc_style->suit_medium);
	  src_x = rotated_src_x;
	  dest_y = fc_style->y4;
	  COMPOSE_ELEMENT (suit_medium_rotated);
 	  dest_x = fc_style->x0;
	  COMPOSE_ELEMENT (suit_medium_rotated);
	  src_x = normal_src_x;
	  if (value == GDECK_NINE)
	    {
	      dest_x = fc_style->x1;
	      dest_y = fc_style->y3;
	      COMPOSE_ELEMENT (fc_style->suit_medium);
	    }
	  else
	    {
	      dest_x = fc_style->x1;
	      dest_y = fc_style->y1;
	      COMPOSE_ELEMENT (fc_style->suit_medium);
	      src_x = rotated_src_x;
	      dest_y = fc_style->y5;
	      COMPOSE_ELEMENT (suit_medium_rotated);
	    }
	  break;
	default:
	  g_assert_not_reached ();
	}
    }
}

  static void
gdeck_real_style_fc_generate_cards_helper (GDeckStyle *style,
					   GError     *error)
{
  GDeckStyleFC *fc_style = GDECK_STYLE_FC (style);
  GdkPixbuf *rank, *rank_rotated;
  GdkPixbuf *suit_small, *suit_small_rotated;
  GdkPixbuf *suit_medium, *suit_medium_rotated;
  GdkPixbuf *suit_large;
  GdkPixbuf *honor, *honor_rotated;
  gint suit, value;

  rank= gdk_pixbuf_composite_color_simple (fc_style->rank,
					   gdk_pixbuf_get_width (fc_style->rank),
					   gdk_pixbuf_get_height (fc_style->rank),
					   GDK_INTERP_BILINEAR,
					   255, 2, 0xFFFFFFFF, 0xFFFFFFFF);
  rank_rotated = gdk_pixbuf_rotate_180 (rank);

  suit_small = gdk_pixbuf_composite_color_simple (fc_style->suit_small,
						  gdk_pixbuf_get_width (fc_style->suit_small),
						  gdk_pixbuf_get_height (fc_style->suit_small),
						  GDK_INTERP_BILINEAR,
						  255, 2, 0xFFFFFFFF, 0xFFFFFFFF);
  suit_small_rotated = gdk_pixbuf_rotate_180 (suit_small);

  suit_medium_rotated = gdk_pixbuf_rotate_180 (fc_style->suit_medium);

  suit_large = gdk_pixbuf_composite_color_simple (fc_style->suit_large,
						  gdk_pixbuf_get_width (fc_style->suit_large),
						   gdk_pixbuf_get_height (fc_style->suit_large),
						  GDK_INTERP_BILINEAR,
						  255, 2, 0xFFFFFFFF, 0xFFFFFFFF);
  honor = gdk_pixbuf_composite_color_simple (fc_style->honor,
						   gdk_pixbuf_get_width (fc_style->honor),
						   gdk_pixbuf_get_height (fc_style->honor),
						   GDK_INTERP_BILINEAR,
						   255, 2, 0xFFFFFFFF, 0xFFFFFFFF);
  honor_rotated = gdk_pixbuf_rotate_180 (honor);

  for (suit = GDECK_CLUB; suit < GDECK_NUM_SUITS; suit ++)
    for (value = GDECK_ACE; value < GDECK_NUM_VALUES; value ++)
      {
	fc_style->cards[suit][value] = gdk_pixbuf_copy (gdeck_style_get_blank (GDECK_STYLE (fc_style)));
	gdeck_real_style_fc_draw_rank (fc_style, rank, rank_rotated, suit, value);
	gdeck_real_style_fc_draw_suit_small (fc_style, suit_small, suit_small_rotated, suit, value);

	if (value == GDECK_ACE || value == GDECK_ACE_HIGH)
	  gdeck_real_style_fc_draw_ace (fc_style, suit_large, suit, value);
	else if (value >= GDECK_TWO && value <= GDECK_TEN)
	  gdeck_real_style_fc_draw_number_cards (fc_style, suit_medium_rotated, suit, value);
	else if (value >= GDECK_JACK && value <= GDECK_KING)
	  gdeck_real_style_fc_draw_honors (fc_style, honor, honor_rotated, suit, value);
      }

  gdk_pixbuf_unref (rank_rotated);
  gdk_pixbuf_unref (suit_small_rotated);
  gdk_pixbuf_unref (suit_medium_rotated);
  gdk_pixbuf_unref (honor_rotated);
}

static void
gdeck_real_style_fc_calculate_dimensions (GDeckStyleFC *fc_style)
{
  gint suit_medium_width = gdk_pixbuf_get_width (fc_style->suit_medium)/4;
  gint suit_medium_height = gdk_pixbuf_get_height (fc_style->suit_medium);
  gint suit_small_width = gdk_pixbuf_get_width (fc_style->suit_small)/4;
  gint rank_width = gdk_pixbuf_get_width (fc_style->rank)/14;
  gint rank_height = gdk_pixbuf_get_height (fc_style->rank)/2;

  fc_style->x1 = (WIDTH - suit_medium_width) / 2;
  fc_style->y3 = (HEIGHT - suit_medium_height) / 2;

  fc_style->x0 = fc_style->x1 - XDELTA;
  fc_style->x2 = fc_style->x1 + XDELTA;

  fc_style->y0 = fc_style->y3 - 3 * YDELTA;
  fc_style->y1 = fc_style->y3 - 2 * YDELTA;
  fc_style->y2 = fc_style->y3 - 1 * YDELTA;
  fc_style->y4 = fc_style->y3 + 1 * YDELTA;
  fc_style->y5 = fc_style->y3 + 2 * YDELTA;
  fc_style->y6 = fc_style->y3 + 3 * YDELTA;

  fc_style->y7 = fc_style->y3 - (3 * YDELTA) / 2;
  fc_style->y8 = fc_style->y3 + (3 * YDELTA) / 2;

  fc_style->sx = fc_style->x0 + (suit_medium_width - suit_small_width) / 2 - XDELTA;
  fc_style->sy = fc_style->y0 + (suit_medium_width - suit_small_width) / 2;
  fc_style->rx = fc_style->sx + (suit_small_width - rank_width) / 2;
  fc_style->ry = fc_style->sy - rank_height - 1;
  if (fc_style->ry < 6) {
    fc_style->ry = 6;
    fc_style->sy = fc_style->ry + rank_height + 1;
  }
}

static void
gdeck_real_style_fc_generate_cards (GDeckStyle *style,
				    GError     *error)
{
  GDeckStyleFC *fc_style = GDECK_STYLE_FC (style);
  gchar *file;

  if (fc_style->need_recalc == FALSE)
    return;

  if (fc_style->back == NULL)
    {
      file = g_strdup_printf ("%s%c%s%c%s",
			      fc_style->card_prefix,
			      G_DIR_SEPARATOR,
			      fc_style->back_dir,
			      G_DIR_SEPARATOR,
			      fc_style->back_file);
      fc_style->back = gdk_pixbuf_new_from_file (file, NULL);
      g_free (file);
    }

  if (fc_style->joker == NULL)
    {
      file = g_strdup_printf ("%s%c%s%c%s",
			      fc_style->card_prefix,
			      G_DIR_SEPARATOR,
			      fc_style->joker_dir,
			      G_DIR_SEPARATOR,
			      fc_style->joker_file);
      fc_style->joker = gdk_pixbuf_new_from_file (file, NULL);
      g_free (file);
    }

  if (fc_style->honor == NULL)
    {
      file = g_strdup_printf ("%s%c%s%c%s",
			      fc_style->card_prefix,
			      G_DIR_SEPARATOR,
			      fc_style->honor_dir,
			      G_DIR_SEPARATOR,
			      fc_style->honor_file);
      fc_style->honor = gdk_pixbuf_new_from_file (file, NULL);
      g_free (file);
    }

  if (fc_style->rank == NULL)
    {
      file = g_strdup_printf ("%s%c%s%c%s",
			      fc_style->card_prefix,
			      G_DIR_SEPARATOR,
			      fc_style->rank_dir,
			      G_DIR_SEPARATOR,
			      fc_style->rank_file);
      fc_style->rank = gdk_pixbuf_new_from_file (file, NULL);
      g_free (file);
    }

  if (fc_style->suit_small == NULL)
    {
      file = g_strdup_printf ("%s%c%s%c%s",
			      fc_style->card_prefix,
			      G_DIR_SEPARATOR,
			      fc_style->suit_small_dir,
			      G_DIR_SEPARATOR,
			      fc_style->suit_small_file);
      fc_style->suit_small = gdk_pixbuf_new_from_file (file, NULL);
      g_free (file);
    }

  if (fc_style->suit_medium == NULL)
    {
      file = g_strdup_printf ("%s%c%s%c%s",
			      fc_style->card_prefix,
			      G_DIR_SEPARATOR,
			      fc_style->suit_medium_dir,
			      G_DIR_SEPARATOR,
			      fc_style->suit_medium_file);
      fc_style->suit_medium = gdk_pixbuf_new_from_file (file, NULL);
      g_free (file);
    }

  if (fc_style->suit_large == NULL)
    {
      file = g_strdup_printf ("%s%c%s%c%s",
			      fc_style->card_prefix,
			      G_DIR_SEPARATOR,
			      fc_style->suit_large_dir,
			      G_DIR_SEPARATOR,
			      fc_style->suit_large_file);
      fc_style->suit_large = gdk_pixbuf_new_from_file (file, NULL);
      g_free (file);
    }

  gdeck_real_style_fc_calculate_dimensions (fc_style);

  if (GDECK_STYLE_DEFAULT (style)->blank == NULL)
    {
      gdeck_style_default_init_outline (GDECK_STYLE_DEFAULT (style));
      gdeck_style_default_init_slots (GDECK_STYLE_DEFAULT (style));

      /* force the blank to be created */
      gdeck_style_get_blank (style);
    }

  if (fc_style->card_back == NULL)
    {
      fc_style->card_back = gdk_pixbuf_copy (gdeck_style_get_blank (style));

      gdk_pixbuf_composite (fc_style->back,
			    fc_style->card_back,
			    0, 0, style->width, style->height,
			    0, 0,
			    (gfloat)style->width/gdk_pixbuf_get_width (fc_style->back),
			    (gfloat)style->height/gdk_pixbuf_get_height (fc_style->back),
			    GDK_INTERP_BILINEAR,
			    255);

    }

  gdeck_real_style_fc_generate_cards_helper (style, error);
  fc_style->need_recalc = FALSE;
}
