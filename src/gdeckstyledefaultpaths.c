/*
 * gdeckstyledefaultpaths.c
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gdeckstyledefault.h"
#include "libart_lgpl/art_vpath.h"
#include "libart_lgpl/art_bpath.h"
#include "libart_lgpl/art_vpath_bpath.h"
#include "libart_lgpl/art_svp.h"
#include "libart_lgpl/art_svp_vpath.h"
#include "libart_lgpl/art_rgb_svp.h"


void
gdeck_style_default_init_outline (GDeckStyleDefault *style)
{
  ArtBpath bpath[19];
  ArtVpath *vpath_temp;
  double x0, y0, x1, y1;
  double radius;
  double halfwidth;
  style->radius = 5;
  
  x0 = 1.0;
  y0 = 1.0;
  x1 = (double) GDECK_STYLE (style)->width - 1.0;
  y1 = (double) GDECK_STYLE (style)->height - 1.0;
  radius = (double) style->radius;
  halfwidth = 0.5;

  bpath[0].code = ART_MOVETO;
  bpath[0].x1 = x0 - halfwidth;
  bpath[0].y1 = y0 + radius;
  bpath[0].x2 = x0 - halfwidth;
  bpath[0].y2 = y0 + radius;
  bpath[0].x3 = x0 - halfwidth;
  bpath[0].y3 = y0 + radius;
  bpath[1].code = ART_LINETO;
  bpath[1].x1 = x0 - halfwidth;
  bpath[1].y1 = y0 + radius;
  bpath[1].x2 = x0 - halfwidth;
  bpath[1].y2 = y1 - radius;
  bpath[1].x3 = x0 - halfwidth;
  bpath[1].y3 = y1 - radius;
  bpath[2].code = ART_CURVETO;
  bpath[2].x1 = x0 - halfwidth;
  bpath[2].y1 = y1 - radius;
  bpath[2].x2 = x0 - halfwidth;
  bpath[2].y2 = y1 + halfwidth;
  bpath[2].x3 = x0 + halfwidth + radius;
  bpath[2].y3 = y1 + halfwidth;
  bpath[3].code = ART_LINETO;
  bpath[3].x1 = x0 + radius;
  bpath[3].y1 = y1 + halfwidth;
  bpath[3].x2 = x1 - radius;
  bpath[3].y2 = y1 + halfwidth;
  bpath[3].x3 = x1 - radius;
  bpath[3].y3 = y1 + halfwidth;
  bpath[4].code = ART_CURVETO;
  bpath[4].x1 = x1 - radius;
  bpath[4].y1 = y1 + halfwidth;
  bpath[4].x2 = x1 + halfwidth;
  bpath[4].y2 = y1 + halfwidth;
  bpath[4].x3 = x1 + halfwidth;
  bpath[4].y3 = y1 - radius;
  bpath[5].code = ART_LINETO;
  bpath[5].x1 = x1 + halfwidth;
  bpath[5].y1 = y1 - radius;
  bpath[5].x2 = x1 + halfwidth;
  bpath[5].y2 = y0 + radius;
  bpath[5].x3 = x1 + halfwidth;
  bpath[5].y3 = y0 + radius;
  bpath[6].code = ART_CURVETO;
  bpath[6].x1 = x1 + halfwidth;
  bpath[6].y1 = y0 + radius;
  bpath[6].x2 = x1 + halfwidth;
  bpath[6].y2 = y0 - halfwidth;
  bpath[6].x3 = x1 - radius;
  bpath[6].y3 = y0 - halfwidth;
  bpath[7].code = ART_LINETO;
  bpath[7].x1 = x1 - radius;
  bpath[7].y1 = y0 - halfwidth;
  bpath[7].x2 = x0 + radius;
  bpath[7].y2 = y0 - halfwidth;
  bpath[7].x3 = x0 + radius;
  bpath[7].y3 = y0 - halfwidth;
  bpath[8].code = ART_CURVETO;
  bpath[8].x1 = x0 + radius;
  bpath[8].y1 = y0 - halfwidth;
  bpath[8].x2 = x0 - halfwidth;
  bpath[8].y2 = y0 - halfwidth;
  bpath[8].x3 = x0 - halfwidth;
  bpath[8].y3 = y0 + radius;

  /* now we need to reverse the path on the inside */
  bpath[9].code = ART_MOVETO;
  bpath[9].x1 = x0 + halfwidth;
  bpath[9].y1 = y0 + radius;
  bpath[9].x2 = x0 + halfwidth;
  bpath[9].y2 = y0 + radius;
  bpath[9].x3 = x0 + halfwidth;
  bpath[9].y3 = y0 + radius;
  bpath[10].code = ART_CURVETO;
  bpath[10].x1 = x0 + halfwidth;
  bpath[10].y1 = y0 + radius;
  bpath[10].x2 = x0 + 2 * halfwidth;
  bpath[10].y2 = y0 + 2 * halfwidth;
  bpath[10].x3 = x0 + radius;
  bpath[10].y3 = y0 + halfwidth;
  bpath[11].code = ART_LINETO;
  bpath[11].x1 = x0 + radius;
  bpath[11].y1 = y0 + halfwidth;
  bpath[11].x2 = x1 - radius;
  bpath[11].y2 = y0 + halfwidth;
  bpath[11].x3 = x1 - radius;
  bpath[11].y3 = y0 + halfwidth;
  bpath[12].code = ART_CURVETO;
  bpath[12].x1 = x1 - radius;
  bpath[12].y1 = y0 + halfwidth;
  bpath[12].x2 = x1 - 2 * halfwidth;
  bpath[12].y2 = y0 + 2 * halfwidth;
  bpath[12].x3 = x1 - halfwidth;
  bpath[12].y3 = y0 + radius;
  bpath[13].code = ART_LINETO;
  bpath[13].x1 = x1 - halfwidth;
  bpath[13].y1 = y0 + radius;
  bpath[13].x2 = x1 - halfwidth;
  bpath[13].y2 = y1 - radius;
  bpath[13].x3 = x1 - halfwidth;
  bpath[13].y3 = y1 - radius;
  bpath[14].code = ART_CURVETO;
  bpath[14].x1 = x1 - halfwidth;
  bpath[14].y1 = y1 - radius;
  bpath[14].x2 = x1 - 2 * halfwidth;
  bpath[14].y2 = y1 - 2 * halfwidth;
  bpath[14].x3 = x1 - radius;
  bpath[14].y3 = y1 - halfwidth;
  bpath[15].code = ART_LINETO;
  bpath[15].x1 = x1 - radius;
  bpath[15].y1 = y1 - halfwidth;
  bpath[15].x2 = x0 + radius;
  bpath[15].y2 = y1 - halfwidth;
  bpath[15].x3 = x0 + radius;
  bpath[15].y3 = y1 - halfwidth;
  bpath[16].code = ART_CURVETO;
  bpath[16].x1 = x0 + radius;
  bpath[16].y1 = y1 - halfwidth;
  bpath[16].x2 = x0 + 2 * halfwidth;
  bpath[16].y2 = y1 - 2 * halfwidth;
  bpath[16].x3 = x0 + halfwidth;
  bpath[16].y3 = y1 - radius;
  bpath[17].code = ART_LINETO;
  bpath[17].x1 = x0 + halfwidth;
  bpath[17].y1 = y1 - radius;
  bpath[17].x2 = x0 + halfwidth;
  bpath[17].y2 = y0 + radius;
  bpath[17].x3 = x0 + halfwidth;
  bpath[17].y3 = y0 + radius;
  bpath[18].code = ART_END;
  bpath[18].x1 = 0;
  bpath[18].y1 = 0;
  bpath[18].x2 = 0;
  bpath[18].y2 = 0;
  bpath[18].x3 = 0;
  bpath[18].y3 = 0;

  vpath_temp = art_bez_path_to_vec (bpath, 0.25);
  style->outline_svp = art_svp_from_vpath (vpath_temp);
  art_free (vpath_temp);

  bpath[0].code = ART_MOVETO;
  bpath[0].x1 = x0;
  bpath[0].y1 = y0 + radius;
  bpath[0].x2 = x0;
  bpath[0].y2 = y0 + radius;
  bpath[0].x3 = x0;
  bpath[0].y3 = y0 + radius;
  bpath[1].code = ART_LINETO;
  bpath[1].x1 = x0;
  bpath[1].y1 = y0 + radius;
  bpath[1].x2 = x0;
  bpath[1].y2 = y1 - radius;
  bpath[1].x3 = x0;
  bpath[1].y3 = y1 - radius;
  bpath[2].code = ART_CURVETO;
  bpath[2].x1 = x0;
  bpath[2].y1 = y1 - radius;
  bpath[2].x2 = x0;
  bpath[2].y2 = y1;
  bpath[2].x3 = x0 + radius;
  bpath[2].y3 = y1;
  bpath[3].code = ART_LINETO;
  bpath[3].x1 = x0 + radius;
  bpath[3].y1 = y1;
  bpath[3].x2 = x1 - radius;
  bpath[3].y2 = y1;
  bpath[3].x3 = x1 - radius;
  bpath[3].y3 = y1;
  bpath[4].code = ART_CURVETO;
  bpath[4].x1 = x1 - radius;
  bpath[4].y1 = y1;
  bpath[4].x2 = x1;
  bpath[4].y2 = y1;
  bpath[4].x3 = x1;
  bpath[4].y3 = y1 - radius;
  bpath[5].code = ART_LINETO;
  bpath[5].x1 = x1;
  bpath[5].y1 = y1 - radius;
  bpath[5].x2 = x1;
  bpath[5].y2 = y0 + radius;
  bpath[5].x3 = x1;
  bpath[5].y3 = y0 + radius;
  bpath[6].code = ART_CURVETO;
  bpath[6].x1 = x1;
  bpath[6].y1 = y0 + radius;
  bpath[6].x2 = x1;
  bpath[6].y2 = y0;
  bpath[6].x3 = x1 - radius;
  bpath[6].y3 = y0;
  bpath[7].code = ART_LINETO;
  bpath[7].x1 = x1 - radius;
  bpath[7].y1 = y0;
  bpath[7].x2 = x0 + radius;
  bpath[7].y2 = y0;
  bpath[7].x3 = x0 + radius;
  bpath[7].y3 = y0;
  bpath[8].code = ART_CURVETO;
  bpath[8].x1 = x0 + radius;
  bpath[8].y1 = y0;
  bpath[8].x2 = x0;
  bpath[8].y2 = y0;
  bpath[8].x3 = x0;
  bpath[8].y3 = y0 + radius;
  bpath[9].code = ART_END;
  bpath[9].x1 = 0;
  bpath[9].y1 = 0;
  bpath[9].x2 = 0;
  bpath[9].y2 = 0;
  bpath[9].x3 = 0;
  bpath[9].y3 = 0;

  vpath_temp = art_bez_path_to_vec (bpath, 0.25);
  style->fill_svp = art_svp_from_vpath (vpath_temp);
  art_free (vpath_temp);
}

void
gdeck_style_default_init_slots (GDeckStyleDefault *style)
{
  ArtBpath bpath[19];
  ArtVpath vpath[11];
  ArtVpath *vpath_temp;
  double x0, y0, x1, y1;
  double radius;
  double halfwidth = 0.5;
  gdouble stock_width;
  gdouble waste_width;
  style->radius = 5;

  x0 = GDECK_STYLE (style)->width/4;
  y0 = GDECK_STYLE (style)->height/2 - x0;
  x1 = 3 * x0;
  y1 = y0 + 2 * x0;
  stock_width = 5;

  bpath[0].code = ART_MOVETO;
  bpath[0].x1 = (x0 + x1)/2;
  bpath[0].y1 = y0;
  bpath[0].x2 = (x0 + x1)/2;
  bpath[0].y2 = y0;
  bpath[0].x3 = (x0 + x1)/2;
  bpath[0].y3 = y0;
  bpath[1].code = ART_CURVETO;
  bpath[1].x1 = (x0 + x1)/2;
  bpath[1].y1 = y0;
  bpath[1].x2 = x0;
  bpath[1].y2 = y0;
  bpath[1].x3 = x0;
  bpath[1].y3 = (y0 + y1)/2;
  bpath[2].code = ART_CURVETO;
  bpath[2].x1 = x0;
  bpath[2].y1 = (y0 + y1)/2;
  bpath[2].x2 = x0;
  bpath[2].y2 = y1;
  bpath[2].x3 = (x0 + x1)/2;
  bpath[2].y3 = y1;
  bpath[3].code = ART_CURVETO;
  bpath[3].x1 = (x0 + x1)/2;
  bpath[3].y1 = y1;
  bpath[3].x2 = x1;
  bpath[3].y2 = y1;
  bpath[3].x3 = x1;
  bpath[3].y3 = (y0 + y1)/2;
  bpath[4].code = ART_CURVETO;
  bpath[4].x1 = x1;
  bpath[4].y1 = (y0 + y1)/2;
  bpath[4].x2 = x1;
  bpath[4].y2 = y0;
  bpath[4].x3 = (x0 + x1)/2;
  bpath[4].y3 = y0;
  bpath[5].code = ART_MOVETO;
  bpath[5].x1 = (x0 + x1)/2;
  bpath[5].y1 = y0 + stock_width;
  bpath[5].x2 = (x0 + x1)/2;
  bpath[5].y2 = y0 + stock_width;
  bpath[5].x3 = (x0 + x1)/2;
  bpath[5].y3 = y0 + stock_width;
  bpath[6].code = ART_CURVETO;
  bpath[6].x1 = (x0 + x1)/2;
  bpath[6].y1 = y0 + stock_width;
  bpath[6].x2 = x1 - stock_width;
  bpath[6].y2 = y0 + stock_width;
  bpath[6].x3 = x1 - stock_width;
  bpath[6].y3 = (y0 + y1)/2;
  bpath[7].code = ART_CURVETO;
  bpath[7].x1 = x1 - stock_width;
  bpath[7].y1 = (y0 + y1)/2;
  bpath[7].x2 = x1 - stock_width;
  bpath[7].y2 = y1 - stock_width;
  bpath[7].x3 = (x0 + x1)/2;
  bpath[7].y3 = y1 - stock_width;
  bpath[8].code = ART_CURVETO;
  bpath[8].x1 = (x0 + x1)/2;
  bpath[8].y1 = y1 - stock_width;
  bpath[8].x2 = x0 + stock_width;
  bpath[8].y2 = y1 - stock_width;
  bpath[8].x3 = x0 + stock_width;
  bpath[8].y3 = (y0 + y1)/2;
  bpath[9].code = ART_CURVETO;
  bpath[9].x1 = x0 + stock_width;
  bpath[9].y1 = (y0 + y1)/2;
  bpath[9].x2 = x0 + stock_width;
  bpath[9].y2 = y0 + stock_width;
  bpath[9].x3 = (x0 + x1)/2;
  bpath[9].y3 = y0 + stock_width;
  bpath[10].code = ART_END;
  bpath[10].x1 = 0;
  bpath[10].y1 = 0;
  bpath[10].x2 = 0;
  bpath[10].y2 = 0;
  bpath[10].x3 = 0;
  bpath[10].y3 = 0;

  vpath_temp = art_bez_path_to_vec (bpath, 0.25);
  style->stock_svp = art_svp_from_vpath (vpath_temp);
  art_free (vpath_temp);

  x0 = GDECK_STYLE (style)->width/4;
  y0 = GDECK_STYLE (style)->height/2 - x0;
  x1 = 3 * x0;
  y1 = y0 + 2 * x0;
  waste_width = stock_width * 1.414;
  
  vpath[0].code = ART_MOVETO;
  vpath[0].x = (x0 + x1)/2;
  vpath[0].y = y0;
  vpath[1].code = ART_LINETO;
  vpath[1].x = x0;
  vpath[1].y = (y0 + y1)/2;
  vpath[2].code = ART_LINETO;
  vpath[2].x = (x0 + x1)/2;
  vpath[2].y = y1;
  vpath[3].code = ART_LINETO;
  vpath[3].x = x1;
  vpath[3].y = (y0 + y1)/2;
  vpath[4].code = ART_LINETO;
  vpath[4].x = (x0 + x1)/2;
  vpath[4].y = y0;
  vpath[5].code = ART_MOVETO;
  vpath[5].x = (x0 + x1)/2;
  vpath[5].y = y0 + waste_width;
  vpath[6].code = ART_LINETO;
  vpath[6].x = x1 - waste_width;
  vpath[6].y = (y0 + y1)/2;
  vpath[7].code = ART_LINETO;
  vpath[7].x = (x0 + x1)/2;
  vpath[7].y = y1 - waste_width;
  vpath[8].code = ART_LINETO;
  vpath[8].x = x0 + waste_width;
  vpath[8].y = (y0 + y1)/2;
  vpath[9].code = ART_LINETO;
  vpath[9].x = (x0 + x1)/2;
  vpath[9].y = y0 + waste_width;
  vpath[10].code = ART_END;
  vpath[10].x = 0;
  vpath[10].y = 0;

  style->waste_svp = art_svp_from_vpath (vpath);
}



