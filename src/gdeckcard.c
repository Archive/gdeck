/*
 * gdeckcard.c
 *
 * Copyright (C) 2001 Jonathan Blandford  <jrb@alum.mit.edu>
 * All rights reserved.
 *
 * This file is part of the gdeck Library.
 *
 * The gdeck library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The gdeck library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gdeckcard.h"

GType
gdeck_card_get_type (void)
{
  static GType card_type = 0;

  if (! card_type)
    {
      card_type = g_boxed_type_register_static ("GDeckCard",
						(GBoxedCopyFunc) gdeck_card_copy,
						(GBoxedFreeFunc) gdeck_card_free);
    }

  return card_type;
}

GDeckCard *
gdeck_card_new (GDeckValue value,
		GDeckSuit suit,
		gboolean face_up)
{
  GDeckCard *card = g_new0 (GDeckCard, 1);

  card->value = value;
  card->suit = suit;
  card->face_up = face_up;

  return card;
}

GDeckCard *
gdeck_card_copy (GDeckCard *card)
{
  g_return_val_if_fail (card != NULL, NULL);

  return gdeck_card_new (card->value, card->suit, card->face_up);
}

void
gdeck_card_free (GDeckCard *card)
{
  g_free (card);
}

void
gdeck_card_flip (GDeckCard *card)
{
  g_return_if_fail (card != NULL);

  card->face_up = !card->face_up;
}

GList *
gdeck_deck_new (gboolean face_up)
{
  GList *retval = NULL;
  GDeckSuit suit = GDECK_CLUB;
  GDeckValue value = GDECK_TWO;

  while (TRUE)
    {
      retval = g_list_prepend (retval, gdeck_card_new (value, suit, face_up));
      if (suit == GDECK_SPADE && value == GDECK_ACE_HIGH)
	break;
      if (value == GDECK_ACE_HIGH)
	{
	  value = GDECK_TWO;
	  suit++;
	}
      else
	{
	  value++;
	}
    }

  return retval;			
}
